<?php
/**
 * Drush alias for local builds of http://www.rheumatoidarthritis.com/
 * Provides the alias:
 * @ra.local = 'http://ra:8080'
 *
 * This is just a template, provide your own values bellow.
 * For more information and remote aliases see ra.aliases.drushrc.php
 */

// @ra.local = 'http://ra:8080'
$aliases['ra.local'] = array(
  'root' => '/Users/matt/Sites/ra/docroot',
  'path-aliases' => array(
    '%dump-dir' => '/tmp/',
  ),
);
$aliases['ra.local']['uri'] = 'http://ra:8080';