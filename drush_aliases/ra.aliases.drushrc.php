<?php
/**
 * Drush alias file for rebuild of http://www.rheumatoidarthritis.com/.  Provides the following aliases:
 * @ra.dev    = 'http://blinkdev3dev.devcloud.acquia-sites.com/' 
 * @ra.stage  = 'http://blinkdev3test.devcloud.acquia-sites.com/'
 * @ra.qa     = 'http://blinkdev3.devcloud.acquia-sites.com/'
 *
 * Usage:
 * 1. Put this file in the directory ~/.drush
 * 2. Download the ppk file found here:
 *    https://ac.blinkreaction.com/public/index.php?path_info=projects%2F101%2Fpages%2F106482
 *    and add it to your authentication agent (e.g. 'ssh-add /path/to/file.ppk').
 *    Alternatively, you can add your PUBLIC key here https://insight.acquia.com/cloud/users?s=222746
 * 3. Invoke with 'drush @ra.dev', 'drush @ra.stage', etc.
 *
 */

// http://blinkdev3dev.devcloud.acquia-sites.com/
$aliases['ra.dev'] = array(
  'root' => '/var/www/html/blinkdev3dev/docroot',
  'remote-host' => 'srv-181.devcloud.hosting.acquia.com',
  'remote-user' => 'blinkdev3',
  'path-aliases' => array(
    '%dump-dir' => '/tmp/',
    '%drush-script' => '/usr/local/bin/drush5',
  ),
);
$aliases['ra.dev']['uri'] = 'blinkdev3dev.devcloud.acquia-sites.com';

// @ra.stage  = 'http://blinkdev3test.devcloud.acquia-sites.com/'
$aliases['ra.stage'] = array(
  'root' => '/var/www/html/blinkdev3test/docroot',
  'remote-host' => 'srv-181.devcloud.hosting.acquia.com',
  'remote-user' => 'blinkdev3',
  'path-aliases' => array(
    '%dump-dir' => '/tmp/',
    '%drush-script' => '/usr/local/bin/drush5',
  ),
);
$aliases['ra.stage']['uri'] = 'blinkdev3test.devcloud.acquia-sites.com';

// @ra.qa     = 'http://blinkdev3.devcloud.acquia-sites.com/'
$aliases['ra.qa'] = array(
  'root' => '/var/www/html/blinkdev3/docroot',
  'remote-host' => 'srv-181.devcloud.hosting.acquia.com',
  'remote-user' => 'blinkdev3',
  'path-aliases' => array(
    '%dump-dir' => '/tmp/',
    '%drush-script' => '/usr/local/bin/drush5',
  ),
);
$aliases['ra.qa']['uri'] = 'blinkdev3.devcloud.acquia-sites.com';

