#Information for creating patches.

For this project all patches should go in the 'patches' folder (i.e. the folder which is a _sibling_ to docroot).

##Path specification
Patches stored in this folder should use the following path specification:
    
    diff --git a/docroot/path/to/my/file.php b/docroot/path/to/my/file.php
    index b75d594..1990f58 100644
    --- a/docroot/path/to/my/file.php
    +++ b/docroot/path/to/my/file.php
    @@ -61,14 +61,15 @@

Where **a and b are siblings of the `.git` folder.**

##Naming
Name patches `123456-description-of-my-changes.patch` where `123456` is the drupal.org issue number.  If there is no drupal issue number (e.g. a patch which you will not be contributing) use 000000-description-of-my-changes.patch.

##Applying patches
To apply a patch give the following commands from the terminial:

    $ cd /your/absolute/path/ra
    # Note: "/your/absolute/path/ra" should be the *git* root.
    $ git apply patches/123456--description-of-my-changes.patch


##Contributing back patches.
If you want to contribute a patch, the version you upload to drupal.org must be relative to the *project* root.

    diff --git a/path/to/my/file.php b/path/to/my/file.php
    index b75d594..1990f58 100644
    --- a/path/to/my/file.php
    +++ b/path/to/my/file.php
    @@ -61,14 +61,15 @@

Where **a and b are siblings of the `my_project.info` file.**