<?php
/**
 * @file
 * ra_webform.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ra_webform_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'Sign Up',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'a046b5b2-09e3-4e2b-8411-a23db5c385fe',
  'type' => 'webform',
  'language' => 'und',
  'created' => '1365153632',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '554962ee-2ff8-40a5-a7ec-d0159f1cec66',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p><strong><span style="line-height: 1.538em;">Sign Up to Get More From RheumatoidArthritis.com</span></strong></p>
<p><small>By registering for this site, you are indicating that you want to receive information from Genentech via mail, e-mail, or text on RA, expert advice for living well with RA, upcoming events, and product information about specific treatments.</small></p>
<div>Fill out the form to get:</div>
<div>&bull; Our one-of-a-kind treatment tracking journal&nbsp;</div>
<div>&bull; Exclusive tips and advice</div>
<div><em>All fields must be completed unless marked as optional.</em></div>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p><strong><span style="line-height: 1.538em;">Sign Up to Get More From RheumatoidArthritis.com</span></strong></p>
<p><small>By registering for this site, you are indicating that you want to receive information from Genentech via mail, e-mail, or text on RA, expert advice for living well with RA, upcoming events, and product information about specific treatments.</small></p>
<div>Fill out the form to get:</div>
<div>• Our one-of-a-kind treatment tracking journal </div>
<div>• Exclusive tips and advice</div>
<div><em>All fields must be completed unless marked as optional.</em></div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_webform_extra' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => '486',
    'confirmation' => '<h2><span style="font-size:18px;"><span style="color: rgb(51, 51, 51); font-family: \'Lucida Grande\', Verdana, Verdana, Arial, Helvetica, sans-serif; line-height: 18px;">Thank you for signing up!</span></span></h2>
<p><span style="color: rgb(51, 51, 51); font-family: \'Lucida Grande\', Verdana, Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;">You should be receiving your first communication from us shortly.</span></p>
',
    'confirmation_format' => 'full_html',
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1',
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1',
      1 => '2',
    ),
    'emails' => array(
      1 => array(
        'nid' => '486',
        'eid' => '1',
        'email' => '13',
        'subject' => 'Thank you for enrolling',
        'from_name' => 'DoNotReply@RheumatoidArthritis.com',
        'from_address' => 'default',
        'template' => '%value[content_information][first_name] %value[content_information][last_name],
Thank you for your interest in RheumatoidArthritis.com! Be on the lookout for RA information and tips headed your way shortly.

© 2013 Genentech USA, Inc All rights reserved. 
1 DNA Way, So. San Francisco, CA 94080-4990',
        'excluded_components' => array(
          0 => '15',
        ),
        'html' => '0',
        'attachments' => '0',
      ),
    ),
    'components' => array(
      1 => array(
        'nid' => 486,
        'cid' => '1',
        'pid' => '0',
        'form_key' => 'content_information',
        'name' => 'Contact Information',
        'type' => 'fieldset',
        'value' => '',
        'extra' => array(
          'addmore' => 0,
          'title_display' => 0,
          'private' => 0,
          'collapsible' => 0,
          'collapsed' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '0',
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 486,
        'cid' => '2',
        'pid' => '1',
        'form_key' => 'first_name',
        'name' => 'First Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'prefix' => '123',
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '1',
        'page_num' => 1,
      ),
      13 => array(
        'nid' => 486,
        'cid' => '13',
        'pid' => '1',
        'form_key' => 'email',
        'name' => 'Email',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '2',
        'page_num' => 1,
      ),
      3 => array(
        'nid' => 486,
        'cid' => '3',
        'pid' => '1',
        'form_key' => 'last_name',
        'name' => 'Last Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '3',
        'page_num' => 1,
      ),
      10 => array(
        'nid' => 486,
        'cid' => '10',
        'pid' => '1',
        'form_key' => 'street_address_1',
        'name' => 'Street Address 1',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '4',
        'page_num' => 1,
      ),
      11 => array(
        'nid' => 486,
        'cid' => '11',
        'pid' => '1',
        'form_key' => 'street_address_2',
        'name' => 'Street Address 2 (Optional)',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '5',
        'page_num' => 1,
      ),
      14 => array(
        'nid' => 486,
        'cid' => '14',
        'pid' => '1',
        'form_key' => 'year_of_birth',
        'name' => 'Year of Birth',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'description' => 'You must be 18 years or older to receive information.',
          'items' => '0|1895
1|1896
2|1897
3|1898
4|1899
5|1900
6|1901
7|1902
8|1903
9|1904
10|1905
11|1906
12|1907
13|1908
14|1909
15|1910
16|1911
17|1912
18|1913
19|1914
20|1915
21|1916
22|1917
23|1918
24|1919
25|1920
26|1921
27|1922
28|1923
29|1924
30|1925
31|1926
32|1927
33|1928
34|1929
35|1930
36|1931
37|1932
38|1933
39|1934
40|1935
41|1936
42|1937
43|1938
44|1939
45|1940
46|1941
47|1942
48|1943
49|1944
50|1945
51|1946
52|1947
53|1948
54|1949
55|1950
56|1951
57|1952
58|1953
59|1954
60|1955
61|1956
62|1957
63|1958
64|1959
65|1960
66|1961
67|1962
68|1963
69|1964
70|1965
71|1966
72|1967
73|1968
74|1969
75|1970
76|1971
77|1972
78|1973
79|1974
80|1975
81|1976
82|1977
83|1978
84|1979
85|1980
86|1981
87|1982
88|1983
89|1984
90|1985
91|1986
92|1987
93|1988
94|1989
95|1990
96|1991
97|1992
98|1993
99|1994
100|1995
',
          'options_source' => 'birthday_years',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'custom_keys' => FALSE,
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '6',
        'page_num' => 1,
      ),
      24 => array(
        'nid' => 486,
        'cid' => '24',
        'pid' => '1',
        'form_key' => 'zip',
        'name' => 'Zip',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '7',
        'page_num' => 1,
      ),
      19 => array(
        'nid' => 486,
        'cid' => '19',
        'pid' => '1',
        'form_key' => 'mobile_phone',
        'name' => 'Mobile Phone Number (optional)',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '8',
        'page_num' => 1,
      ),
      4 => array(
        'nid' => 486,
        'cid' => '4',
        'pid' => '0',
        'form_key' => 'conditional_information',
        'name' => 'Condition Information',
        'type' => 'fieldset',
        'value' => '',
        'extra' => array(
          'title_display' => 0,
          'private' => 0,
          'collapsible' => 0,
          'collapsed' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '1',
        'page_num' => 1,
      ),
      5 => array(
        'nid' => 486,
        'cid' => '5',
        'pid' => '4',
        'form_key' => 'relationship',
        'name' => 'What is your relationship to the person with RA?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'description' => '<strong>Please answer the following questions as they relate to the person with RA.</strong>',
          'items' => '1|I’m the person with RA 
2|I’m a family member / friend / care partner',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '2',
        'page_num' => 1,
      ),
      7 => array(
        'nid' => 486,
        'cid' => '7',
        'pid' => '4',
        'form_key' => 'diadnose_type',
        'name' => 'What type of RA have you been diagnosed with?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|Mild
2|Moderate
3|Severe',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '3',
        'page_num' => 1,
      ),
      8 => array(
        'nid' => 486,
        'cid' => '8',
        'pid' => '4',
        'form_key' => 'ever_taken',
        'name' => 'Have you ever taken any of the following medicines?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|Yes
2|No',
          'multiple' => 0,
          'prefix' => '<ul>
<li>Methotrexate</li>
<li>Orencia® (abatacept)</li>
<li>Xeljanz® (tofacitinib)</li>
<li>Plaquenil® (hydroxychloroquine)</li>
<li>Arava® (leflunomide)</li>
<li>Azulfidine® (sulfasalazine)</li>
<li>Imuran® (azathioprine)</li>
<li>Kineret® (anakinra)</li>
<li>Other DMARDs</li>
</ul>',
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '4',
        'page_num' => 1,
      ),
      16 => array(
        'nid' => 486,
        'cid' => '16',
        'pid' => '4',
        'form_key' => 'ever_taken_medicines',
        'name' => 'Have you ever taken any of the following medicines?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '11|Yes
12|No',
          'multiple' => 0,
          'prefix' => '<ul>
<li>Enbrel® (etanercept)</li>
<li>Humira® (adalimumab)</li>
<li>Remicade® (infliximab)</li>
<li>Simponi® (golimumab)</li>
<li>Cimzia® (certolizumab pegol)</li>
</ul>',
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '5',
        'page_num' => 1,
      ),
      22 => array(
        'nid' => 486,
        'cid' => '22',
        'pid' => '4',
        'form_key' => 'medicine_fieldset',
        'name' => 'What medicine(s) are you CURRENTLY using to treat your RA?',
        'type' => 'fieldset',
        'value' => '',
        'extra' => array(
          'addmore' => 1,
          'addmore_settings' => array(
            'add_button' => 'Add another medicine',
            'remove_button' => 'Remove medicine',
          ),
          'title_display' => 0,
          'private' => 0,
          'collapsible' => 0,
          'collapsed' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '6',
        'page_num' => 1,
      ),
      23 => array(
        'nid' => 486,
        'cid' => '23',
        'pid' => '22',
        'form_key' => 'medicine_item',
        'name' => 'Current medicine',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '10',
        'page_num' => 1,
      ),
      18 => array(
        'nid' => 486,
        'cid' => '18',
        'pid' => '4',
        'form_key' => 'receive_your_medication',
        'name' => 'Which way would you like to receive your RA medication?',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => '1|By an IV infusion (given to you by a healthcare professional, usually at your doctor\'s office)
2|By an injection
3|No preference',
          'multiple' => 0,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '7',
        'page_num' => 1,
      ),
      15 => array(
        'nid' => 486,
        'cid' => '15',
        'pid' => '4',
        'form_key' => 'about_your_privacy',
        'name' => 'About Your Privacy',
        'type' => 'markup',
        'value' => '<p><strong>About Your Privacy</strong></p>
<p>We understand that protecting the privacy of visitors to our websites is very important and that information about your health is particularly sensitive. Sign up to receive information that can help you play an active role in your own care and gain a better understanding about RA products from Genentech and important RA topics. We only collect personally identifiable information about you if you choose to give it to us. We do not share any of your personally identifiable information with third parties for their own marketing use unless you explicitly give us permission to do so. Please review our <a href="http://www.gene.com/privacy-policy">Privacy Statement</a>&nbsp;to learn more about how we collect, use, share, and protect information online.</p>
<p>By registering for this site, you are indicating that you want to receive information from Genentech via mail, e-mail, or text on RA, expert advice for living well with RA, upcoming events, and product information about specific treatments.&nbsp;</p>
',
        'extra' => array(
          'conditional_operator' => '=',
          'format' => 'full_html',
          'private' => 0,
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '0',
        'weight' => '9',
        'page_num' => 1,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-05 05:20:32 -0400',
  'path' => array(
    'pid' => '754',
    'source' => 'node/486',
    'alias' => 'updates-registration',
    'language' => 'und',
    'pathauto' => FALSE,
    'old_alias' => 'updates-registration',
  ),
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Unsubscribe',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '18a86b84-8934-400c-8d08-cc5bca6a3f71',
  'type' => 'webform',
  'language' => 'und',
  'created' => '1369915845',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '857cdf43-f8b0-4b5c-8760-1c93440f5f31',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>To opt out of receiving RheumatoidArthritis.com communications, we&#39;ll need your e-mail address.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>To opt out of receiving RheumatoidArthritis.com communications, we\'ll need your e-mail address.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_webform_extra' => array(),
  'metatags' => array(
    'und' => array(
      'title' => array(
        'value' => 'Unsubscribe from RheumatoidArthritis.com | Rheumatoid Arthritis',
      ),
      'description' => array(
        'value' => 'Unsubscribe and opt out of receiving updates from RheumatoidArthritis.com.',
      ),
      'keywords' => array(
        'value' => 'unsubscribe, unsubscribe from rheumatoidarthritis.com',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => '607',
    'confirmation' => '<h2 style="color: #474747;">We&rsquo;re sad to see you go</h2>
<p style="color: #474747;">You have successfully unsubscribed from RheumatoidArthritis.com communications.</p>
<p style="color: #474747;">Return to <a href="/" style="color: #48ABBE;">RheumatoidArthritis.com</a>.</p>
',
    'confirmation_format' => 'full_html',
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1',
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1',
      1 => '2',
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 607,
        'cid' => '1',
        'pid' => '0',
        'form_key' => 'email',
        'name' => 'E-mail address* (required)',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'width' => '',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '0',
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 607,
        'cid' => '2',
        'pid' => '0',
        'form_key' => 'unsubscribe_choice',
        'name' => 'Select what you would like to no longer receive:',
        'type' => 'select',
        'value' => '',
        'extra' => array(
          'items' => 'texts|Texts
mail|Mail
monthly_emails|Monthly Emails
all_emails|All Other Emails',
          'multiple' => 1,
          'title_display' => 'before',
          'private' => 0,
          'aslist' => 0,
          'optrand' => 0,
          'conditional_operator' => '=',
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'custom_keys' => FALSE,
          'options_source' => '',
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '1',
        'page_num' => 1,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-05-30 08:10:45 -0400',
  'path' => array(
    'pid' => '1152',
    'source' => 'node/607',
    'alias' => 'unsubscribe',
    'language' => 'und',
    'pathauto' => TRUE,
    'old_alias' => 'unsubscribe',
  ),
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'RA Talk Unsubscribe',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '0168c395-3e9e-40d7-8be7-7672af6cc392',
  'type' => 'webform',
  'language' => 'und',
  'created' => '1376051379',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'c01c8f2a-a4ce-4a95-8750-77fc94af39e8',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>To opt out of receiving RA Talk communications, we&#39;ll need your e-mail address.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>To opt out of receiving RA Talk communications, we\'ll need your e-mail address.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_webform_extra' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => '614',
    'confirmation' => '<h1>We&#39;re sad to see you go.</h1>
<p id="talkunsubscr-body">You have successfully unsubscribed from RA Talk. If you&#39;re interested in signing up for a brand new program, <strong><a href="/">RheumatoidArthritis.com</a></strong> has tips, tools, and info brought to you by people who have RA or work in the RA world.</p>
<div class="talkunsubscr-signup"><a href="/" id="talkunsubscr-logo">RheumatoidArthritis.com</a>
	<p id="talkunsubscr-slogan">Expanding Your RA World</p>
	<a class="talkunsubscr-btn" href="/updates-registration">Sign Up Now</a></div>
',
    'confirmation_format' => 'full_html',
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '0',
    'submit_text' => '',
    'submit_limit' => '-1',
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1',
      1 => '2',
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 614,
        'cid' => '1',
        'pid' => '0',
        'form_key' => 'email',
        'name' => 'E-mail address (required)',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'title_display' => 'none',
          'private' => 0,
          'width' => '40',
          'disabled' => 0,
          'unique' => 0,
          'conditional_operator' => '=',
          'description' => '',
          'attributes' => array(),
          'conditional_component' => '',
          'conditional_values' => '',
        ),
        'mandatory' => '1',
        'weight' => '0',
        'page_num' => 1,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-08-09 08:29:39 -0400',
  'path' => array(
    'pid' => '1240',
    'source' => 'node/614',
    'alias' => 'RATalkUnsubscribe',
    'language' => 'und',
    'pathauto' => FALSE,
    'old_alias' => 'RATalkUnsubscribe',
  ),
);
  return $nodes;
}
