<?php
/**
 * @file
 * ra_poll.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ra_poll_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_poll|node|mpoll|form';
  $field_group->group_name = 'group_poll';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'mpoll';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Poll',
    'weight' => '3',
    'children' => array(
      0 => 'field_mpoll_poll',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_poll|node|mpoll|form'] = $field_group;

  return $export;
}
