<?php
/**
 * @file
 * ra_poll.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ra_poll_user_default_permissions() {
  $permissions = array();

  // Exported permission: admin pollfield field_mpoll_poll.
  $permissions['admin pollfield field_mpoll_poll'] = array(
    'name' => 'admin pollfield field_mpoll_poll',
    'roles' => array(
      'administrator' => 'administrator',
      'site administrator' => 'site administrator',
    ),
    'module' => 'pollfield',
  );

  // Exported permission: view pollfield results field_mpoll_poll.
  $permissions['view pollfield results field_mpoll_poll'] = array(
    'name' => 'view pollfield results field_mpoll_poll',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'site administrator' => 'site administrator',
    ),
    'module' => 'pollfield',
  );

  return $permissions;
}
