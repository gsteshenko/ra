<?php
/**
 * @file
 * ra_inspiration.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ra_inspiration_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:inspiration.
  $config['node:inspiration'] = array(
    'instance' => 'node:inspiration',
    'config' => array(
      'title' => array(
        'value' => 'Daily Inspirational Quotes | Rheumatoid Arthritis',
      ),
      'description' => array(
        'value' => 'Get inspired and get practical tips for living with your rheumatoid arthritis (RA) with these quotes from our RA experts.',
      ),
      'keywords' => array(
        'value' => 'inspirational quotes, daily inspirational quotes',
      ),
      'og:image' => array(
        'value' => '[node:field_image_original]',
      ),
    ),
  );

  return $config;
}
