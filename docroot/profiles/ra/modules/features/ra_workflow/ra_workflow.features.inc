<?php
/**
 * @file
 * ra_workflow.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ra_workflow_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
