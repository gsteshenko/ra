<?php
/**
 * @file
 * ra_basic_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ra_basic_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'Contact Us',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'e0321f1f-4ee8-4beb-98c5-7f094cad4174',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300830',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '0645f766-7346-446f-84e5-4e85da16f7a6',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Maecenas at dolor mi, et posuere nibh. Integer eget sem auctor lectus bibendum aliquet. Donec metus dolor, varius in tristique eget, faucibus quis quam. Pellentesque vehicula blandit ipsum sit amet euismod. Aenean pulvinar, felis vel hendrerit sodales, felis justo posuere risus, sed tincidunt neque augue ut eros. Mauris lacinia molestie elit, vel pretium nisl cursus in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
<p>Integer et tristique odio. Maecenas vestibulum pellentesque venenatis. Aliquam volutpat dolor in urna venenatis a lacinia lacus congue. Quisque eget leo a turpis porta placerat vel non lectus. Sed posuere nisi augue, ornare ullamcorper neque. Aliquam tellus lacus, egestas at suscipit non, tempus eu enim. Pellentesque nec velit justo. Sed congue leo pretium turpis pretium nec venenatis quam accumsan. Ut imperdiet sodales nibh, et ultricies est malesuada sed. Sed malesuada iaculis lobortis. Integer vel lectus neque, nec pellentesque elit. Sed bibendum sollicitudin aliquam. Nullam vitae ligula quis felis posuere ornare.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Maecenas at dolor mi, et posuere nibh. Integer eget sem auctor lectus bibendum aliquet. Donec metus dolor, varius in tristique eget, faucibus quis quam. Pellentesque vehicula blandit ipsum sit amet euismod. Aenean pulvinar, felis vel hendrerit sodales, felis justo posuere risus, sed tincidunt neque augue ut eros. Mauris lacinia molestie elit, vel pretium nisl cursus in. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
<p>Integer et tristique odio. Maecenas vestibulum pellentesque venenatis. Aliquam volutpat dolor in urna venenatis a lacinia lacus congue. Quisque eget leo a turpis porta placerat vel non lectus. Sed posuere nisi augue, ornare ullamcorper neque. Aliquam tellus lacus, egestas at suscipit non, tempus eu enim. Pellentesque nec velit justo. Sed congue leo pretium turpis pretium nec venenatis quam accumsan. Ut imperdiet sodales nibh, et ultricies est malesuada sed. Sed malesuada iaculis lobortis. Integer vel lectus neque, nec pellentesque elit. Sed bibendum sollicitudin aliquam. Nullam vitae ligula quis felis posuere ornare.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 12:00:30 -0400',
  'path' => array(
    'pid' => '876',
    'source' => 'node/509',
    'alias' => 'contact-us',
    'language' => 'und',
    'pathauto' => TRUE,
    'old_alias' => 'contact-us',
  ),
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Join Now',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '2eae12d1-d6c2-44db-ab64-f58012e54cc6',
  'type' => 'page',
  'language' => 'und',
  'created' => '1376304612',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'bef0342c-0ec6-420b-82f6-21dd68d36538',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div id="gto-landing-page">
	<div id="gto-logo">&nbsp;</div>
	<div id="sign-up-wrapper">
		<div class="shadow-wrapper-vert">&nbsp;</div>
		<div class="signup left">
			<p class="header-1">sign up FOR Free</p>
			<p class="header-2">RHEUMATOID <span>ARTHRITIS</span></p>
			<p class="header-3">Support</p>
		</div>
		<div class="signup right">
			<p>Living with rheumatoid arthritis and joint pain can be challenging. But you don&#39;t have to do it alone. An online community of support is waiting for you.</p>
			<a href="updates-registration">Sign Up Now &gt;</a></div>
	</div>
	<div id="gto-list">
		<p>Here&#39;s what you&#39;ll receive:</p>
		<ul>
			<li id="item-1">Advice and inspiration from rheumatoid arthritis experts and people who have it</li>
			<li id="item-2">Support for managing your rheumatoid arthritis</li>
			<li id="item-3">Our one-of-a-kind treatment tracking journal</li>
		</ul>
	</div>
	<div id="gto-footer">
		<div id="gto-footer-logo">&nbsp;</div>
		<div id="gto-copyright"><span>Copyright &copy;2013</span></div>
		<ul>
			<li><a href="http://www.rocheusa.com/portal/usa/privacy">Privacy Policy</a></li>
			<li><a href="http://www.rocheusa.com/portal/usa/legal">Terms and Conditions</a></li>
			<li><a href="http://www.rocheusa.com/portal/usa/contact">Contact Us</a></li>
		</ul>
	</div>
</div>
<p>&nbsp;</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div id="gto-landing-page">
<div id="gto-logo"> </div>
<div id="sign-up-wrapper">
<div class="shadow-wrapper-vert"> </div>
<div class="signup left">
<p class="header-1">sign up FOR Free</p>
<p class="header-2">RHEUMATOID <span>ARTHRITIS</span></p>
<p class="header-3">Support</p>
</div>
<div class="signup right">
<p>Living with rheumatoid arthritis and joint pain can be challenging. But you don\'t have to do it alone. An online community of support is waiting for you.</p>
<p>			<a href="updates-registration">Sign Up Now &gt;</a></p></div>
</div>
<div id="gto-list">
<p>Here\'s what you\'ll receive:</p>
<ul><li id="item-1">Advice and inspiration from rheumatoid arthritis experts and people who have it</li>
<li id="item-2">Support for managing your rheumatoid arthritis</li>
<li id="item-3">Our one-of-a-kind treatment tracking journal</li>
</ul></div>
<div id="gto-footer">
<div id="gto-footer-logo"> </div>
<div id="gto-copyright"><span>Copyright ©2013</span></div>
<ul><li><a href="http://www.rocheusa.com/portal/usa/privacy">Privacy Policy</a></li>
<li><a href="http://www.rocheusa.com/portal/usa/legal">Terms and Conditions</a></li>
<li><a href="http://www.rocheusa.com/portal/usa/contact">Contact Us</a></li>
</ul></div>
</div>
<p> </p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-08-12 06:50:12 -0400',
  'path' => array(
    'pid' => '1241',
    'source' => 'node/615',
    'alias' => 'JoinNow',
    'language' => 'und',
    'pathauto' => FALSE,
    'old_alias' => 'JoinNow',
  ),
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Privacy Policy',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '3a7e5563-d71b-4541-9c3b-1418448665ea',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300593',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'd7f751c7-9f6c-44b1-ad76-78d02fad13de',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Pellentesque eu lacus ut enim accumsan volutpat eu non ipsum. Mauris enim mauris, tristique in rhoncus ultricies, semper quis purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ornare malesuada lorem non feugiat. Aenean consectetur nulla quis ante rutrum commodo. Maecenas commodo justo accumsan orci euismod sollicitudin. Mauris libero lectus, pulvinar sit amet scelerisque in, sollicitudin at libero.</p>
<p>In magna diam, scelerisque a consectetur in, laoreet quis neque. Nullam eget arcu magna, sed viverra odio. Praesent et arcu non purus tristique dictum. Sed id nunc in urna volutpat vestibulum ut a urna. Etiam eu orci nisi. Duis egestas imperdiet neque, ac laoreet est ultricies eu. Curabitur fringilla, elit ut pharetra elementum, arcu erat elementum neque, id condimentum sem nulla id urna. Maecenas a ligula dolor. Suspendisse mattis semper consequat. Quisque at turpis sit amet dui sagittis imperdiet.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Pellentesque eu lacus ut enim accumsan volutpat eu non ipsum. Mauris enim mauris, tristique in rhoncus ultricies, semper quis purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ornare malesuada lorem non feugiat. Aenean consectetur nulla quis ante rutrum commodo. Maecenas commodo justo accumsan orci euismod sollicitudin. Mauris libero lectus, pulvinar sit amet scelerisque in, sollicitudin at libero.</p>
<p>In magna diam, scelerisque a consectetur in, laoreet quis neque. Nullam eget arcu magna, sed viverra odio. Praesent et arcu non purus tristique dictum. Sed id nunc in urna volutpat vestibulum ut a urna. Etiam eu orci nisi. Duis egestas imperdiet neque, ac laoreet est ultricies eu. Curabitur fringilla, elit ut pharetra elementum, arcu erat elementum neque, id condimentum sem nulla id urna. Maecenas a ligula dolor. Suspendisse mattis semper consequat. Quisque at turpis sit amet dui sagittis imperdiet.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 11:56:33 -0400',
  'path' => array(
    'pid' => '873',
    'source' => 'node/506',
    'alias' => 'privacy-policy',
    'language' => 'und',
    'pathauto' => TRUE,
    'old_alias' => 'privacy-policy',
  ),
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Terms and Conditions',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'e6b716bd-e03a-4147-8e35-87c5d9a4ec73',
  'type' => 'page',
  'language' => 'und',
  'created' => '1366300706',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'f71a2ac4-1867-425a-ba16-f222bb246627',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Mauris porta suscipit sem ac mattis. Suspendisse ullamcorper leo vitae turpis hendrerit at luctus sapien consectetur. Vivamus vitae metus eget sapien volutpat elementum in sed lectus. Integer mauris libero, egestas tempus tempus vel, vulputate sit amet nibh. Cras viverra mollis fermentum. Cras eget purus sed nisl cursus accumsan. Sed hendrerit condimentum pretium. In hac habitasse platea dictumst. Nunc nunc urna, vulputate id posuere sit amet, imperdiet et orci. Quisque semper vulputate ante, sit amet volutpat nulla mollis volutpat. Sed ligula tellus, facilisis ut bibendum eget, posuere at ipsum</p>
<p>Sed massa lectus, facilisis sed dapibus at, pharetra id leo. Nulla condimentum, ipsum sodales venenatis mattis, arcu risus imperdiet ipsum, quis laoreet tortor arcu luctus velit. Suspendisse nec est sapien. Cras dignissim dapibus massa a euismod. Mauris consequat mi vitae mauris feugiat a rutrum odio porttitor. Sed iaculis augue vitae orci aliquet bibendum. Cras porttitor placerat condimentum.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Mauris porta suscipit sem ac mattis. Suspendisse ullamcorper leo vitae turpis hendrerit at luctus sapien consectetur. Vivamus vitae metus eget sapien volutpat elementum in sed lectus. Integer mauris libero, egestas tempus tempus vel, vulputate sit amet nibh. Cras viverra mollis fermentum. Cras eget purus sed nisl cursus accumsan. Sed hendrerit condimentum pretium. In hac habitasse platea dictumst. Nunc nunc urna, vulputate id posuere sit amet, imperdiet et orci. Quisque semper vulputate ante, sit amet volutpat nulla mollis volutpat. Sed ligula tellus, facilisis ut bibendum eget, posuere at ipsum</p>
<p>Sed massa lectus, facilisis sed dapibus at, pharetra id leo. Nulla condimentum, ipsum sodales venenatis mattis, arcu risus imperdiet ipsum, quis laoreet tortor arcu luctus velit. Suspendisse nec est sapien. Cras dignissim dapibus massa a euismod. Mauris consequat mi vitae mauris feugiat a rutrum odio porttitor. Sed iaculis augue vitae orci aliquet bibendum. Cras porttitor placerat condimentum.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_image' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'name' => 'admin',
  'picture' => '0',
  'data' => 'a:1:{s:7:"overlay";i:0;}',
  'print_mail_display' => 0,
  'print_mail_display_comment' => 0,
  'print_mail_display_urllist' => 0,
  'date' => '2013-04-18 11:58:26 -0400',
  'path' => array(
    'pid' => '874',
    'source' => 'node/507',
    'alias' => 'terms-and-conditions',
    'language' => 'und',
    'pathauto' => TRUE,
    'old_alias' => 'terms-and-conditions',
  ),
);
  return $nodes;
}
