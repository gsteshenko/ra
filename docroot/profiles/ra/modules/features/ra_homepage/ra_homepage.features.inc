<?php
/**
 * @file
 * ra_homepage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ra_homepage_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function ra_homepage_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: featured_carousel
  $nodequeues['featured_carousel'] = array(
    'name' => 'featured_carousel',
    'title' => 'Featured Carousel',
    'subqueue_title' => '',
    'size' => '3',
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => '1',
    'show_in_tab' => '1',
    'show_in_links' => '0',
    'reference' => '0',
    'reverse' => '0',
    'i18n' => '0',
    'types' => array(
      0 => 'article',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}
