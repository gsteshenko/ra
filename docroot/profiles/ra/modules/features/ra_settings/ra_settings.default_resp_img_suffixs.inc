<?php
/**
 * @file
 * ra_settings.default_resp_img_suffixs.inc
 */

/**
 * Implements hook_default_resp_img_suffixs().
 */
function ra_settings_default_resp_img_suffixs() {
  $export = array();

  $resp_img_suffix = new stdClass();
  $resp_img_suffix->disabled = FALSE; /* Edit this to true to make a default resp_img_suffix disabled initially */
  $resp_img_suffix->api_version = 1;
  $resp_img_suffix->name = 'desktop';
  $resp_img_suffix->label = 'Desktop';
  $resp_img_suffix->suffix = '_desktop';
  $resp_img_suffix->breakpoint = 1024;
  $export['desktop'] = $resp_img_suffix;

  $resp_img_suffix = new stdClass();
  $resp_img_suffix->disabled = FALSE; /* Edit this to true to make a default resp_img_suffix disabled initially */
  $resp_img_suffix->api_version = 1;
  $resp_img_suffix->name = 'mobile';
  $resp_img_suffix->label = 'Mobile';
  $resp_img_suffix->suffix = '_mobile';
  $resp_img_suffix->breakpoint = 1;
  $export['mobile'] = $resp_img_suffix;

  $resp_img_suffix = new stdClass();
  $resp_img_suffix->disabled = FALSE; /* Edit this to true to make a default resp_img_suffix disabled initially */
  $resp_img_suffix->api_version = 1;
  $resp_img_suffix->name = 'tablet';
  $resp_img_suffix->label = 'Tablet';
  $resp_img_suffix->suffix = '_tablet';
  $resp_img_suffix->breakpoint = 641;
  $export['tablet'] = $resp_img_suffix;

  return $export;
}
