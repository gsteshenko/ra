<?php
/**
 * @file
 * ra_video.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ra_video_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:video.
  $config['node:video'] = array(
    'instance' => 'node:video',
    'config' => array(
      'title' => array(
        'value' => 'Rheumatoid Arthritis (RA) Videos | [node:field-video-category]',
      ),
      'description' => array(
        'value' => 'Discover the RAise Your Voice animated video series and learn about living with rheumatoid arthritis (RA) from RA community experts.',
      ),
      'keywords' => array(
        'value' => 'rheumatoid arthritis videos, raise your voice, creaky joints, rheumatoid patient foundation, ra warrior, kelly young, lisa emrich, Katie stewart',
      ),
    ),
  );

  return $config;
}
