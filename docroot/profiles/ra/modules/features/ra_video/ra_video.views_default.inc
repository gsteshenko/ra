<?php
/**
 * @file
 * ra_video.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ra_video_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'videos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_video_video']['id'] = 'field_video_video';
  $handler->display->display_options['fields']['field_video_video']['table'] = 'field_data_field_video_video';
  $handler->display->display_options['fields']['field_video_video']['field'] = 'field_video_video';
  $handler->display->display_options['fields']['field_video_video']['label'] = '';
  $handler->display->display_options['fields']['field_video_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_video_video']['settings'] = array(
    'file_view_mode' => 'preview',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );

  /* Display: Videos List */
  $handler = $view->new_display('block', 'Videos List', 'videos_list');
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['text'] = '[field_image]<div class="video-play-button"></div>';
  $handler->display->display_options['fields']['field_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'video_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_video_video']['id'] = 'field_video_video';
  $handler->display->display_options['fields']['field_video_video']['table'] = 'field_data_field_video_video';
  $handler->display->display_options['fields']['field_video_video']['field'] = 'field_video_video';
  $handler->display->display_options['fields']['field_video_video']['label'] = '';
  $handler->display->display_options['fields']['field_video_video']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_video_video']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_video_video']['alter']['link_class'] = 'video-thumb-link';
  $handler->display->display_options['fields']['field_video_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_video']['empty'] = '[field_image]';
  $handler->display->display_options['fields']['field_video_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_video_video']['settings'] = array(
    'file_view_mode' => 'default',
  );
  /* Field: Content: Is new */
  $handler->display->display_options['fields']['field_video_new']['id'] = 'field_video_new';
  $handler->display->display_options['fields']['field_video_new']['table'] = 'field_data_field_video_new';
  $handler->display->display_options['fields']['field_video_new']['field'] = 'field_video_new';
  $handler->display->display_options['fields']['field_video_new']['label'] = '';
  $handler->display->display_options['fields']['field_video_new']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_new']['hide_empty'] = TRUE;
  /* Field: Play Button */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Play Button';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="video-play-button"></div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Video Category (field_video_category) */
  $handler->display->display_options['arguments']['field_video_category_tid']['id'] = 'field_video_category_tid';
  $handler->display->display_options['arguments']['field_video_category_tid']['table'] = 'field_data_field_video_category';
  $handler->display->display_options['arguments']['field_video_category_tid']['field'] = 'field_video_category_tid';
  $handler->display->display_options['arguments']['field_video_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_video_category_tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['field_video_category_tid']['default_argument_options']['term_page'] = FALSE;
  $handler->display->display_options['arguments']['field_video_category_tid']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['field_video_category_tid']['default_argument_options']['limit'] = TRUE;
  $handler->display->display_options['arguments']['field_video_category_tid']['default_argument_options']['vocabularies'] = array(
    'video_category' => 'video_category',
  );
  $handler->display->display_options['arguments']['field_video_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_video_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_video_category_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Video Contributors */
  $handler = $view->new_display('block', 'Video Contributors', 'video_contributors');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<h2>Meet the Cast of "[title_1]"</h2>';
  $handler->display->display_options['header']['area']['format'] = 'script';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Contributors (field_video_contributors) - reverse */
  $handler->display->display_options['relationships']['reverse_field_video_contributors_node']['id'] = 'reverse_field_video_contributors_node';
  $handler->display->display_options['relationships']['reverse_field_video_contributors_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_video_contributors_node']['field'] = 'reverse_field_video_contributors_node';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'contributors_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Contributor Info */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Contributor Info';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="contributor-info">
<div class="views-field-field-image">[field_image]</div>
<div class="views-field-title">[title]</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Content: Bio Summary */
  $handler->display->display_options['fields']['field_contributor_bio_summary']['id'] = 'field_contributor_bio_summary';
  $handler->display->display_options['fields']['field_contributor_bio_summary']['table'] = 'field_data_field_contributor_bio_summary';
  $handler->display->display_options['fields']['field_contributor_bio_summary']['field'] = 'field_contributor_bio_summary';
  $handler->display->display_options['fields']['field_contributor_bio_summary']['label'] = '';
  $handler->display->display_options['fields']['field_contributor_bio_summary']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_contributor_bio_summary']['element_label_colon'] = FALSE;
  /* Field: Trimmed Bio */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['ui_name'] = 'Trimmed Bio';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
  );
  /* Field: Full Bio */
  $handler->display->display_options['fields']['body_1']['id'] = 'body_1';
  $handler->display->display_options['fields']['body_1']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body_1']['field'] = 'body';
  $handler->display->display_options['fields']['body_1']['ui_name'] = 'Full Bio';
  $handler->display->display_options['fields']['body_1']['label'] = '';
  $handler->display->display_options['fields']['body_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body_1']['settings'] = array(
    'trim_length' => '100',
  );
  /* Field: Bio Mobile */
  $handler->display->display_options['fields']['body_2']['id'] = 'body_2';
  $handler->display->display_options['fields']['body_2']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body_2']['field'] = 'body';
  $handler->display->display_options['fields']['body_2']['ui_name'] = 'Bio Mobile';
  $handler->display->display_options['fields']['body_2']['label'] = '';
  $handler->display->display_options['fields']['body_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body_2']['alter']['text'] = '[body_2]';
  $handler->display->display_options['fields']['body_2']['element_label_colon'] = FALSE;
  /* Field: Show/Hide Full Bio Mobile */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Show/Hide Full Bio Mobile';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<a href="/#" class="full-bio-mobile">Show Full Bio</a>
<a href="/#" class="minimize-mobile">Minimize</a>';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Field: Current Video Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'reverse_field_video_contributors_node';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Current Video Title';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Contributors (field_video_contributors:delta) */
  $handler->display->display_options['sorts']['delta']['id'] = 'delta';
  $handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_video_contributors';
  $handler->display->display_options['sorts']['delta']['field'] = 'delta';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_video_contributors_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contributor' => 'contributor',
  );
  $export['videos'] = $view;

  return $export;
}
