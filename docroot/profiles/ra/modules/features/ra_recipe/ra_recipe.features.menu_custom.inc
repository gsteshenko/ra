<?php
/**
 * @file
 * ra_recipe.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ra_recipe_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-recipe-menu.
  $menus['menu-recipe-menu'] = array(
    'menu_name' => 'menu-recipe-menu',
    'title' => 'Recipe Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Recipe Menu');


  return $menus;
}
