<?php
/**
 * @file
 * ra_recipe.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ra_recipe_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:recipe.
  $config['node:recipe'] = array(
    'instance' => 'node:recipe',
    'config' => array(
      'title' => array(
        'value' => '[node:title] | Recipe for RA | Rheumatoid Arthritis',
      ),
      'description' => array(
        'value' => 'Learn how to make [node:title], a rheumatoid arthritis (RA) friendly recipe.',
      ),
      'keywords' => array(
        'value' => '[node:title], ra friendly recipe, recipes for ra, ra recipes, recipes for rheumatoid arthritis',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
    ),
  );

  return $config;
}
