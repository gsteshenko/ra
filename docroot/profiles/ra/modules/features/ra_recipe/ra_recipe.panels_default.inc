<?php
/**
 * @file
 * ra_recipe.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function ra_recipe_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'virtual_cookbook';
  $mini->category = '';
  $mini->admin_title = 'Virtual Cookbook';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'one';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'The RA-Friendly Virtual Cookbook';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1';
    $pane->panel = 'one_main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(),
    );
    $pane->configuration = array(
      'admin_title' => 'Virtual Cookbook',
      'title' => '',
      'body' => '<div class="vc-description">Browse delicious easy-to-make recipes with step-by-step instructions for whipping up a yummy meal without putting stress on your joints. The recipes included are RA-friendly in 3 different ways. Use the classification below as your guide:</div>
<ul class="vc-properties">
	<li class="vc-property-1">Easy to make</li>
	<li class="vc-property-2">No or low in inflammatory ingredients</li>
	<li class="vc-property-3">Antioxidant-rich/reduces inflammation</li>
</ul>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $display->content['new-1'] = $pane;
    $display->panels['one_main'][0] = 'new-1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['virtual_cookbook'] = $mini;

  return $export;
}
