<?php
/**
 * @file ra_soap.admin.inc
 * TODO: Enter file description here.
 */

/**
 * RA soap settings form.
 */
function ra_soap_admin_settings($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['ra_soap_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable SOAP client'),
    '#description' => t('Enable sending submissions subscribe and unsubscribe forms'),
    '#default_value' => variable_get('ra_soap_enabled', FALSE),
  );

  $form['ra_soap_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Debug mode'),
    '#default_value' => variable_get('ra_soap_debug', FALSE),
  );

  $form['ra_soap_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WSDL URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('ra_soap_wsdl', FALSE),
  );

  $form['ra_soap_interaction_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('QRS Interaction ID Prefix'),
    '#required' => TRUE,
    '#default_value' => variable_get('ra_soap_interaction_prefix', 'QRSBUNDLE'),
  );

  $form['ra_campaign_codes'] = array();
  $element = &$form['ra_campaign_codes'];

  $values = variable_get('ra_campaign_codes', array(
    'source_code' => '',
    'website_id' => '',
    'campaign_code' => '',
    'promotion_code' => '',
    'kit_code' => '',
    'product_code' => '',
    'program_code' => '',
    'offer_code' => '',
    'media_origin_code' => '',
    'vendor_code' => '',
  ));

  $element['source_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Source Code'),
    '#required' => TRUE,
    '#default_value' => $values['source_code'],
  );
  $element['website_id'] = array(
    '#type' => 'textfield',
    '#title' => t('WebSiteID'),
    '#required' => TRUE,
    '#default_value' => $values['website_id'],
  );
  $element['campaign_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign Code'),
    '#required' => TRUE,
    '#default_value' => $values['campaign_code'],
  );
  $element['promotion_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Promotion Code'),
    '#required' => TRUE,
    '#default_value' => $values['promotion_code'],
  );
  $element['kit_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Kit Code'),
    '#required' => TRUE,
    '#default_value' => $values['kit_code'],
  );
  $element['product_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Product Code'),
    '#required' => TRUE,
    '#default_value' => $values['product_code'],
  );
  $element['program_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Program Code'),
    '#required' => TRUE,
    '#default_value' => $values['program_code'],
  );
  $element['offer_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Offer Code'),
    '#required' => TRUE,
    '#default_value' => $values['offer_code'],
  );
  $element['media_origin_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Media Origin Code'),
    '#required' => TRUE,
    '#default_value' => $values['media_origin_code'],
  );
  $element['vendor_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Vendor Code'),
    '#required' => TRUE,
    '#default_value' => $values['vendor_code'],
  );

  return system_settings_form($form);
}

/**
 * RA soap testing form.
 */
function ra_soap_admin_testing($form, &$form_state) {
  $form['form_nid'] = array(
    '#type' => 'select',
    '#title' => t('Form'),
    '#options' => ra_soap_form_handlers_mapper(),
    '#required' => TRUE,
   );

  $form['submissions'] = array(
    '#type' => 'textarea',
    '#title' => t('Test submissions'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
  );

  return $form;
}

/**
 * Callback on form submission.
 */
function ra_soap_admin_testing_submit($form, &$form_state) {
  $strings = explode("\n", $form_state['values']['submissions']);
  $form_nid = $form_state['values']['form_nid'];
  $num_lines = count($strings);

  if ($num_lines) {
    $operations = array();
    for ($i = 0; $i < $num_lines; $i++) {
      $operations[] = array('ra_soap_testing_batch', array($strings[$i], $form_nid, t('(Operation @operation)', array('@operation' => $i))));
    }

    $batch = array(
      'operations' => $operations,
      'finished' => 'ra_soap_testing_batch_finished',
      'title' => t('Processing batch'),
      'init_message' => t('Batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Batch has encountered an error.'),
      'file' => drupal_get_path('module', 'ra_soap').'/ra_soap.admin.inc',
    );

    batch_set($batch);
  }
}

/**
 * Batch operation for to create one submission.
 */
function ra_soap_testing_batch($submission, $form_nid, $operation_details, &$context) {
  if ($submission) {
    _ra_soap_testing_create_submission($submission, $form_nid);

    $context['results'][] = $operation_details;
    $context['message'] = check_plain($node->title);
  }
}

/**
 * Batch 'finished' callback.
 */
function ra_soap_testing_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('@count results processed.', array('@count' => count($results))));
    drupal_set_message(t('The final result was "%final"', array('%final' => end($results))));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}

/**
 * Create new submission.
 */
function _ra_soap_testing_create_submission($string, $form_nid) {
  module_load_include('inc', 'webform', 'includes/webform.components');

  $map = array(
    RA_SOAP_SUBSCRIBE_NID => array(
      1 => 'first_name',
      'last_name',
      'email',
      'year_of_birth',
      'street_address_1',
      'street_address_2',
      9 => 'zip',
      'mobile_phone',
      array('field' => 'relationship', 'val' => '1'),
      array('field' => 'relationship', 'val' => '2'),
      array('field' => 'diadnose_type', 'val' => '3'),
      array('field' => 'diadnose_type', 'val' => '2'),
      array('field' => 'diadnose_type', 'val' => '1'),
      array('field' => 'medicine_item', 'cb' => '_ra_soap_testing_medicine_item'),
      array('field' => 'ever_taken_medicines', 'val' => '11'),
      array('field' => 'ever_taken_medicines', 'val' => '12'),
      array('field' => 'ever_taken', 'val' => '1'),
      array('field' => 'ever_taken', 'val' => '2'),
      array('field' => 'receive_your_medication', 'val' => '1'),
      array('field' => 'receive_your_medication', 'val' => '2'),
      array('field' => 'receive_your_medication', 'val' => '3'),
    ),
    RA_SOAP_UNSUBSCRIBE_NID => array(
      'email',
      array('field' => 'unsubscribe_choice', 'cb' => '_ra_soap_testing_choice_static'),
    ),
    RA_SOAP_OLD_UNSUBSCRIBE_NID => array(
      'email',
    ),
  );

  $values = explode("\t", $string);
  $_REQUEST['moc'] = preg_replace('/^.*\((.*)\)$/i', "$1", $values[0]);

  $form_id = 'webform_client_form_' . $form_nid;
  $wf_form_state = array(
    'values' => array(
      'submitted' => array(),
      'op' => t('Submit'),
    ),
  );
  $node = node_load($form_nid);
  $submission = (object) array();

  $components = array();
  foreach ($node->webform['components'] as $comp) {
    $components[$comp['form_key']] = $comp['cid'];
  }

  if ($map[$form_nid]) {
    foreach ($map[$form_nid] as $idx => $field) {
      if (!empty($values[$idx])) {
        if (is_array($field)) {
          $field_name = $field['field'];

          if (isset($field['cb']) && function_exists($field['cb'])) {
            $value = $field['cb']($values[$idx]);
          }
          else {
            $value = $field['val'];
          }
        }
        else {
          $field_name = $field;
          $value = $values[$idx];
        }

        $cid = $components[$field_name];
        $component = $node->webform['components'][$cid];
        if ($component) {
          $parents = webform_component_parent_keys($node, $component);
          drupal_array_set_nested_value($wf_form_state['values']['submitted']
            , $parents, $value);
        }
      }
    }
  }

  dpm($wf_form_state);

  drupal_form_submit($form_id, $wf_form_state, $node, $submission);
  $form_errors = form_get_errors();
  dpm($form_errors);
}

function _ra_soap_testing_medicine_item($value) {
  $value = preg_replace('/^Y.*\((.*)\)$/i', "$1", $value);
  return array($value);
}

function _ra_soap_testing_choice($value) {
  $choices = array(
    'texts' => 'texts',
    'all_emails' => 'all_emails',
    'mail' => 'mail',
    'monthly_emails' => 'monthly_emails',
  );
  shuffle($choices);

  return array_slice($choices, 0, rand(1, 4));
}

function _ra_soap_testing_choice_static($value) {
  // $choices = array(
  //   'all_emails' => 'all_emails',
  //   'monthly_emails' => 'monthly_emails',
  // );

  // $choices = array(
  //   'texts' => 'texts',
  //   'mail' => 'mail',
  // );

  $choices = array(
    'texts' => 'texts',
    'all_emails' => 'all_emails',
    'mail' => 'mail',
    'monthly_emails' => 'monthly_emails',
  );

  return $choices;
}
