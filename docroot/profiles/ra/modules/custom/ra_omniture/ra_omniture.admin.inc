<?php
/**
 * @file ra_omniture.admin.inc
 * TODO: Enter file description here.
 */

/**
 * RA omniture settings form.
 */
function ra_omniture_admin_settings($form, &$form_state) {
  $form['#tree'] = TRUE;

  $ro = variable_get('ra_omniture', array());
  $form['ra_omniture'] = array();
  $element = &$form['ra_omniture'];

  $element['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode'),
    '#description' => t('In debug mode all tracked links will become non-active and only will output in console s variable on click'),
    '#default_value' => isset($ro['debug']) ? $ro['debug'] : '',
  );

  $form['ra_omniture_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Omniture s_account'),
    '#required' => TRUE,
    '#default_value' => variable_get('ra_omniture_account', ''),
  );

  $element['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('User role tracking'),
    '#collapsible' => TRUE,
    '#description' => t('Define which user roles should be tracked by SiteCatalyst.'),
    '#weight' => '-6',
  );

  $result = db_select('role','r')
    ->fields('r')
    ->orderBy('name', 'ASC')
    ->execute();

  foreach ($result as $role) {
    // Can't use empty spaces in varname.
    $role_varname = str_replace(' ', '_', $role->name);
    // Only the basic roles are translated.
    $role_name = in_array($role->rid, array(DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID)) ? t($role->name) : $role->name;
    $element['roles'][$role_varname] = array(
      '#type' => 'checkbox',
      '#title' => $role_name,
      '#default_value' => isset($ro['roles'][$role_varname]) ? $ro['roles'][$role_varname] : '',
    );
  }

  $default_variables = array(
    // Default variables
    'campaign' => t('Default Campaign Code'),
    'moc' => t('Default MOCode'),
    // Search engine variables
    'search_campaign' => t('Search Campaign Code'),
    'search_moc' => t('Search Campaign Code'),
    // Social variables
    'fb_campaign' => t('Facebook Campaign Code'),
    'fb_moc' => t('Facebook Campaign Code'),
    'twitter_campaign' => t('Twitter Campaign Code'),
    'twitter_moc' => t('Twitter Campaign Code'),
    'pst_campaign' => t('Pinterest Campaign Code'),
    'pst_moc' => t('Pinterest Campaign Code'),
  );

  $element['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Variables'),
    '#collapsible' => TRUE,
  );

  foreach ($default_variables as $var_name => $var_title) {
    $element['default'][$var_name] = array(
      '#type' => 'textfield',
      '#title' => $var_title,
      '#default_value' => isset($ro['default'][$var_name]) ? $ro['default'][$var_name] : '',
    );
  }

  // $element['variables'] = array(
  //   '#type' => 'fieldset',
  //   '#title' => t('Custom Variables'),
  //   '#collapsible' => TRUE,
  //   '#collapsed' => TRUE,
  //   '#description' => t('You can define tracking variables here.'),
  //   '#weight' => '-3',
  // );

  // _ra_omniture_variables_form($element['variables'], $ro);

  return system_settings_form($form);
}
