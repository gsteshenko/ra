var omniture
  , s_account
  , s_extra = {};

;(function($) {
  omniture = (function () {
    var rules = {}
      , processed = [];

    return {
      getDelimiter: function() {
        return ',';
      },
      isProcessed: function(selector) {
        return $.inArray(selector, processed) !== -1;
      },
      setProcessed: function(selector) {
        processed.push(selector);
      },
      removeProcessed: function(selector) {
        processed.splice($.inArray(selector, processed), 1);
      },
      delay: function(delay) {
        var date = new Date();
        var curDate = null;

        do {
          curDate = new Date();
        }
        while (curDate - date < delay);
      },
      debug: function() {
        return Drupal.settings.ra_omniture.debug;
      },
      addRules: function(type, _rules) {
        rules[type] = _rules;
      },
      addComponent: function(type, fn) {
        var pars = {};
        pars[type] = fn(this);

        $.extend(this, pars);
      },
      getDefaults: function() {
        var vars = {}
          , track_vars = ['campaign', 'prop51', 'eVar51']
          , engines = ['www.google.com', 'www.bing.com', 'search.yahoo.com']
          , location = this.tools.parseURL(window.location.href)
          , referrer = this.tools.parseURL(document.referrer)
          , args = window.location.pathname.split('/');

        for (var i = 1; i <= 3; i++) {
          if (args[i]) {
            vars['prop' + i] = args[i].replace(/[\-]/g, ' ');
            track_vars.push('prop' + i);
          }
        }

        if (!location.params.cid && !location.params.c) {
          if ($.inArray(referrer.host, engines) > -1) {
            vars['campaign'] = s_extra.search_campaign;
            vars['prop51'] = vars['eVar51'] = s_extra.search_moc;
          }
          else {
            vars['campaign'] = s_extra.campaign;
            vars['prop51'] = vars['eVar51'] = s_extra.moc;
          }
        }

        return { v: vars, tv: track_vars };
      },
      processVars: function(_o, _vars) {
        var vars = {}
          , $this = this;

        $.each(_vars, function(name, value) {
          var v = '';

          if (typeof value === 'object') {
            if (typeof $this.callbacks[value.cb] === 'function') {
              v = $this.callbacks[value.cb](_o, value, _vars, vars);
            }
          }
          else {
            v = value;
          }

          // Replacing special chars
          if (typeof v === 'string') {
            v = v.replace('& ', 'and ');
          }

          vars[name] = v;
        });


        vars.mode = (_o.href && $this.tools.checkIfExt(_o.href)) ? 'e' : 'o';

        return vars;
      },
      tl: function(_s, target) {
        var s = s_gi(s_account)
          , defaults = this.getDefaults()
          , track_vars = ['prop37', 'eVar37', 'prop38']
          , track_events = []
          , s_linkTrackVarsTemp = s.linkTrackVars
          , s_linkTrackEventsTemp = s.linkTrackEvents;

        if (this.debug()) {
          console.log(_s);
        }

        externalWindowLink = 'false';
        s.linkTrackEvents = 'None';
        $.each(_s, function(n, v) {
          if (n.indexOf('__') === 0) return;

          if (n != 'mode') {
            if (n.indexOf('event') === 0) {
              s.events = v;
              track_vars.push('events');
              track_events.push(v);
            }
            else {
              track_vars.push(n);
              s[n] = v;

              if (!_s.__no_evar) {
                var num = n.replace('prop', '')
                  , evar = 'eVar' + num;

                  track_vars.push(evar);
                  s[evar] = v;
              }
            }
          }
        })
        jQuery.extend(s, defaults.v);
        jQuery.merge(track_vars, defaults.tv);
        if (track_vars) {
          s.linkTrackVars = track_vars.join(',');
        }

        if (track_events) {
          s.linkTrackEvents = s.events = track_events.join(',');
        }

        // Populate prop37-38 if 35-36 exists
        if (_s.prop35 && _s.prop36) {
          s.prop37 = s.eVar37 = _s.prop37 ? _s.prop37 : (_s.prop35 + ' | ' + _s.prop36);

          if (_s.prop38) {
            s.prop38 = _s.prop35 + ' | ' + _s.prop36 + ' | ' + _s.prop38;
          }
          else {
            s.prop38 = _s.prop35 + ' | ' + _s.prop36;
          }
        }
        else {
          s.prop37 = s.prop38 = s.eVar37 = s.eVar38 = '';
        }

        s.tl(target.href ? target : true, _s.mode, s.prop37);
        s.linkTrackVars = s_linkTrackVarsTemp;
        s.linkTrackEvents = s_linkTrackEventsTemp;
      },
      init: function (context) {
        var $this = this
          , is_prod = location.href.indexOf('www.rheumatoidarthritis.com') > -1
          , defaults;

        if (!this.initialized) {
          jQuery.extend(s_extra, Drupal.settings.ra_omniture.s);
          defaults = this.getDefaults();
        }

        if (!this.initialized) {
          jQuery.extend(s, defaults.v);
          var s_code = s.t();

          if (s_code) {
            document.write(s_code);
            s.linkTrackVars = 'None';
            s.linkTrackEvents = 'None';
          }
        }

        $.each(rules, function(type, rules) {
          if (typeof $this.rules[type + 'Rule'] === 'function') {
            $this.rules[type + 'Rule'](rules, context);
          }
        });

        this.initialized = true;
      }
    };
  })();
})(jQuery)

;(function($) {
  omniture.addComponent('tools', function($this) {
    return {
      checkIfExt: function(url) {
        var pattern = /^(([^\/:]+?\.)*)([^\.:]{4,})((\.[a-z]{1,4})*)(:[0-9]{1,5})?$/
          , host = window.location.host.replace(pattern, '$3$4')
          , subdomain = window.location.host.replace(pattern, '$1');

        if (subdomain == 'www.' || subdomain == '') {
          var subdomains = "(www\\.)?";
        }
        else {
          var subdomains = subdomain.replace(".", "\\.");
        }

        var internal_link = new RegExp("^https?://" + subdomains + host, "i");

        return !url.match(internal_link);
      },
      parseURL: function(url) {
        var a =  document.createElement('a');
        a.href = url;

        return {
            source: url,
            protocol: a.protocol.replace(':',''),
            host: a.hostname,
            port: a.port,
            query: a.search,
            params: (function(){
                var ret = {},
                    seg = a.search.replace(/^\?/,'').split('&'),
                    len = seg.length, i = 0, s;
                for (;i<len;i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),
            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
            hash: a.hash.replace('#',''),
            path: a.pathname.replace(/^([^\/])/,'/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
            segments: a.pathname.replace(/^\//,'').split('/')
        };
      },
      formValid: function(form_id) {
        var is_valid = true
          , form_error_fields = [];

        $(form_id).find(":input:not(:hidden):not(:submit)").each(function() {
          var type = $(this).attr("type")
            , name = $(this).attr("name")
            , val = $(this).val()
            , required = true
            , re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

          if (type === 'text' && !$(this).hasClass('required')) {
            required = false;
          }

          if (type === 'email') {
            is_valid = re.test(val);
          }

          if (type === 'radio') {
            val = $(form_id + " input[name='" + $(this).attr("name") + "']:checked").val();
          }

          if (type === 'checkbox') {
            if (name.lastIndexOf('[]') < 0 && name.lastIndexOf('[')) {
              name =  name.substring(0, name.lastIndexOf('['));
            }

            val = $(form_id + " input[name*='" + name + "']:checked").val();
          }

          if (required && !val) {
            is_valid = false;
            form_error_fields.push(name);
          }
        })

        return is_valid ? true : form_error_fields;
      }
    };
  });
})(jQuery)

;(function($) {
  omniture.addComponent('callbacks', function($this) {
    return {
      getText: function(_o, vars, global, processed) {
        var o = $(_o)
          , _default = vars.default || ''
          , prefix = vars.prefix ? vars.prefix + ' ' : '';

        if (vars.parent) {
          o = o.closest(vars.parent);
        }

        if (vars.find) {
          o = o.find(vars.find);
        }

        return prefix + (o.text() || _default);
      },
      concatProcessed: function(_o, vars, global, processed) {
        var variables = vars.vars || []
          , delimiter = vars.delimiter || $this.getDelimiter()
          , values = [];

        $.each(variables, function(i, v) {
          if (processed[v] && typeof processed[v] !== 'object') {
            values.push(processed[v]);
          }
        });

        return values.join(delimiter);
      },
      socialType: function(_o, vars) {
        var social_link = $this.tools.parseURL(_o.href)
          , type = 'Unknown';

        switch (social_link.host) {
          case 'facebook.com':
            type = 'facebook';

            break;
          case 'twitter.com':
            type = 'twitter';

            break;
          case 'pinterest.com':
            type = 'pinterest';

            break;
          default:
            if ($(_o).hasClass('rate-button')) {
              type = 'like';
            }
            else {
              type = 'email';
            }

            break;
        }

        return type;
      },
      counterText: function(_o, vars) {
        var o = $(_o)
          , el = vars.parent === false ? o : o.closest(vars.parent || '.views-row')
          , add = typeof vars.add === 'number' ? vars.add : 1
          , prefix = vars.prefix ? vars.prefix + ' ' : '';

        return prefix + (el.parent().find('> :not(.clone)').index(el) + add);
      },
      getValue: function(_o, vars) {
        var o = (_o.tagName.toLowerCase() != 'form') ? $(_o).closest('form') : $(_o)
          , val
          , values = [];

        o.find(vars.field).each(function() {
          var field = $(this);

          if (vars.parent) field = field.closest(vars.parent);
          if (vars.find) field = field.find(vars.find);

          val = field.is('input') ? field.val() : field.text();

          if (vars.values && vars.values[val]) {
            val = vars.values[val];
          }

          values.push(val.trim());
        })

        return values.join($this.getDelimiter());
      },
      getObjectValue: function(_o, vars, global) {
        var val
          , values = [];

        if (typeof Drupal.settings.ra_omniture !== 'undefined') {
          data = Drupal.settings.ra_omniture['submission'];

          if (data[vars.property]) {
            $.each(data[vars.property], function(idx, val) {
              if (vars.values && vars.values[val]) {
                val = vars.values[val];
              }

              values.push(val.trim());
            });
          }
        }

        return values.join($this.getDelimiter());
      },
      formValidStatus: function(_o, vars, global) {
        return global.valid_status ? global.valid_status.join($this.getDelimiter()) : '';
      },
      extra: function(_o, vars) {
        var prefix = vars.prefix || ''
          , suffix = vars.suffix || ''
          , index = vars.index ? (' ' + ($(vars.index).index($(_o)) + 1)) : '';

        return prefix + s_extra[vars.property] + suffix + index;
      },
      s: function(_o, vars) {
        var prefix = vars.prefix || ''
          , suffix = vars.suffix || '';

        return prefix + s[vars.property] + suffix;
      }
    };
  });
})(jQuery)

;(function($) {
  omniture.addComponent('rules', function($this) {
    return {
      linkRule: function(rules, context) {
        $.each(rules, function(selector, $vars) {
          $(selector).once('omniture-link', function(idx) {
            $(this).bind('click', function(e) {
              var lnk = this;

              $this.tl($this.processVars(lnk, $vars), this);

              e.stopPropagation();
              if ($this.debug()) {
                e.preventDefault();
              }
            })
          })
        });
      },
      formRule: function(rules, context) {
        $.each(rules, function(form_id, $vars) {
          $(form_id).once('omniture-form', function() {
            var $form = this;
            $(this).find('input.form-submit').bind('click', function(e) {
              var submit = this
                , vars_key = 'valid';

              if ($vars.validate) {
                var valid_status = $this.tools.formValid(form_id);

                if (valid_status !== true) {
                  vars_key = 'invalid';
                }

                $vars[vars_key].valid_status = valid_status;
              }

              if ($vars[vars_key]) {
                $this.tl($this.processVars($form, $vars[vars_key]), $form);
              }

              e.stopPropagation();

              if ($this.debug()) {
                e.preventDefault();
              }
            });
          });
        });
      },
      formOnLoadRule: function(rules, context) {
        $.each(rules, function(element, $vars) {
          var $el = $(element)
            , el = $el.length ? $el.get(0) : document
            , form_valid;

          if (typeof Drupal.settings.ra_omniture['submission'] !== 'undefined'
              && Drupal.settings.ra_omniture['submission'][$vars.sub_id]) {
            form_valid = 'valid';
          }
          else if ($el.length && $el.find('input.error').length) {
            form_valid = 'invalid';
          }

          if (form_valid) {
            $this.tl($this.processVars(el, $vars[form_valid]), el);
          }
        });
      },
      ajaxCompleteRule: function(rules, context) {
        $.each(rules, function(element, $vars) {
          $(element).each(function() {
            if (context !== document) {
              if ($(context).attr('id') == $(this).attr('id')) {
                $this.tl($this.processVars(this, $vars), this);
              }
            }
          });
        });
      },
      sliderRule: function(rules, context) {
        $.each(rules, function(slider_id, $vars) {
          $('body').bind(slider_id + 'SliderSlide', function(e, target) {
            $this.tl($this.processVars(target, $vars), target);

            e.stopPropagation();
            if ($this.debug()) {
              e.preventDefault();
            }
          })
        });
      }
    };
  });
})(jQuery)

;(function($) {
  $(function() {
    if (!omniture) return;

    var loc = omniture.tools.parseURL(window.location.href)
      , available_referers = {
        'google': {
          'host_pattern': 'www.google.',
          'code': 'MIUARAPS0100'
        },
        'yahoo': {
          'host_pattern': 'search.yahoo.com',
          'code': 'MIUARAPS0100'
        },
        'bing': {
          'host_pattern': 'bing.com',
          'code': 'MIUARAPS0100'
        },
      }
      , url_params = loc.params;

    if (!url_params.moc && document.referrer) {
      var referrer = omniture.tools.parseURL(document.referrer);

      $.each(available_referers, function(engine, settings) {
        if (referrer.host.indexOf(settings.host_pattern) > -1) {
          url_params.moc = settings.code;
        }
      });
    }

    if (url_params.moc || url_params.c) {
      $.cookie('moc', url_params.moc || url_params.c)
    }
  });
})(jQuery)

Drupal.behaviors.ra_omniture = {
  attach: function(context) {
    try {
      omniture.init(context);
    } catch (e) {
      if (omniture.debug()) {
        console.log(e.message);
      }
    }
  }
};

