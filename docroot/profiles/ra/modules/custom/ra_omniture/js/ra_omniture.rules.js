 (function($) {
  omniture.addRules('link', {
    'a#logo': {
      'prop35': 'Header',
      'prop36': 'Logo'
    },
    '.pane-menu-menu-header-links ul.menu li a': {
      'prop35': 'Header',
      'prop36': { cb: 'getText' },
      'prop6': { cb: 'extra', property: 'pageName', prefix: 'intc=' }
    },
    '.pane-menu-menu-footer-menu ul.menu li a': {
      'prop35': 'Footer',
      'prop36': { cb: 'getText' },
      'prop6': { cb: 'extra', property: 'pageName', prefix: 'intc=' }
    },
    '#footer .pane-menu-menu-topics-menu ul.menu li a': {
      'prop35': 'Footer',
      'prop36': { cb: 'getText' }
    },
    '.pane-menu-menu-copyright-menu ul.menu li a': {
      'prop35': 'Footer',
      'prop36': { cb: 'getText' }
    },
    '.view-inspirations div.social-all a': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': 'Inspiration for the Day'
    },
    '.pane-sign-up div.signup-button a': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText' }
    },
    '.hp-meet-experts div.block-content a': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText' }
    },
    '.hp-ra101 div.block-content a:not(.rate-button)': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': 'RA 101'
    },
    'div.pane-articles.featured-slider ul.slides li a:not(.rate-button)': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: 'li', find: '.views-field-title' },
      'prop38': { cb: 'counterText', prefix: 'Carousel Frame' }
    },
    '#pollfield-voting-form #edit-vote': {
      'events': 'event8',
      'prop16': { cb: 's', property: 'channel', suffix: ':survey-response' },
      'prop35': { cb: 'getText', parent: 'div.pollfield-form', find: '.pollfield-title' },
      'prop36': { cb: 'getValue', parent: '.form-item', field: 'input[name*="choice"]:checked', find: 'label' },
      'prop38': { cb: 'extra', property: 'pageName' },
      'prop28': { cb: 'concatProcessed', vars: ['prop35', 'prop36'], delimiter: ' -- ' }
    },
    'div.view-pledges a.pledge-share': {
      'prop35': 'Monthly Pledge',
      'prop36': 'Link'
    },
    'div.pane-articles.hp-last-articles div.views-row a:not(.rate-button)': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title a' },
      'prop38': { cb: 'counterText', prefix: 'Most Recent Tile' }
    },
    'div.pane-topic-articles div.views-field-view div.views-row a:not(.rate-button)': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title a' },
      'prop38': { cb: 'getText', prefix: 'Most Popular', parent: '.views-row', find: '.views-field-name' }
    },
    // Print
    'a.print': {
      'events': 'event10'
    },
    // Meet the Experts
    'div.view-experts.view-display-id-experts_page div.views-row a': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title' },
      'prop38': { cb: 'counterText', add: 0 }
    },
    // Expert Article article
    'body.node-type-expert div.view-display-id-expert_articles div.views-row a:not(.rate-button)': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title' },
    },
    // Email overlay opened, social links
    'a.ctools-modal-modal-popup-send-email, a.social, div.rate-widget-1 a.rate-button': {
      'prop35': 'Social',
      'prop36': 'Tell a Friend',
      'eVar72': { cb: 'socialType' },
      'events': 'event9'
    },
    // Interstitial Before Brand Sites
    'div.ctools-modal-content.interstitial div.interstitial-buttons a': {
      'prop35': 'Interstitial Before Brand Sites',
      'prop36': { cb: 'getText' },
    },
    // Topic articles click
    'body.topic-landing div.topic-articles div.views-row a': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title' },
      'prop38': { cb: 'counterText' }
    },
    // Article email popup
    '#ra-alters-send-email #edit-btn-submit': {
      'prop35': 'Email Overlay',
      'prop36': 'Send Button'
    },
    // Article goals worksheet
    'body.node-type-article div.field-name-field-article-body a[href*="Goals_Worksheet"]': {
      'prop15': 'doctor discussion guide',
      'prop35': 'Be SMART About Goal Setting',
      'prop36': 'SMART Worksheet',
      'prop37': 'SMART Goal Setting Worksheet',
      'events': 'event7'
    },
    // Article body USDA click
    'body.node-type-article div.field-name-field-article-body a[href*="www.choosemyplate.gov"]': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': 'USDA'
    },
    // Article body sign-up click
    'body.node-type-article div.field-name-field-article-body a[href*="updates-registration"]': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': 'Sign Up Now',
      'prop6': { cb: 'extra', property: 'pageName', prefix: 'intc=', index: 'body.node-type-article div.field-name-field-article-body a[href*="updates-registration"]' }
    },
    // Article prescription callout click
    'body.node-type-article div.field-name-field-article-body a[href*="genentech-offers-2-prescription-treatments-ra"]': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': 'Prescription Treatment Callout'
    },
    'body.node-type-video div.view-display-id-videos_list .views-row a': {
      'prop35': 'Raise Your Voice',
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-title' }
    },
    'body.node-type-video div.view-display-id-video_contributors .views-row a[href*="rawarrior.com"]': {
      'prop35': 'Raise Your Voice',
      'prop36': 'RAWarrior'
    },
    'body.node-type-video div.view-display-id-video_contributors .views-row a[href*="rheum4us.org"]': {
      'prop35': 'Raise Your Voice',
      'prop36': 'RPF'
    },
    'body.node-type-video div.view-display-id-video_contributors .views-row a[href*="www.creakyjoints.org"]': {
      'prop35': 'Raise Your Voice',
      'prop36': 'CreakyJoints'
    },
    'body.node-type-video div.view-display-id-video_contributors .views-row a:not(.ext)': {
      'prop35': 'Raise Your Voice',
      'prop36': { cb: 'getText', parent: '.views-row', find: '.views-field-nothing .views-field-title' }
    }
  });

  omniture.addRules('formOnLoad', {
    '#webform-client-form-486': {
      'sub_id': 'form-486',
      'valid': {
        'prop22': { cb: 'getObjectValue', property: 'relationship', values: { 1: 'Patient', 2: 'Caregiver' }},
        'prop23': { cb: 'getObjectValue', property: 'diadnose_type', values: { 1: 'mild', 2: 'moderate', 3: 'severe' }},
        'prop24': { cb: 'getObjectValue', property: 'ever_taken', values: { 1: 'Yes', 2: 'No' }},
        'prop25': { cb: 'getObjectValue', property: 'ever_taken_medicines', values: { 11: 'Yes', 12: 'No' }},
        'prop26': { cb: 'getObjectValue', property: 'medicine_item' },
        'prop27': { cb: 'getObjectValue', property: 'receive_your_medication', values: { 1: 'infusion', 2: 'injection', 3: 'no preference' }},

        'events': 'event2,event3,event12',
        'prop11': { cb: 's', property: 'channel', suffix: ':sign up' },
        'prop13': { cb: 's', property: 'channel', suffix: ':get more info' },

        'prop35': 'Sign Up',
        'prop36': 'Submit'
      },
      'invalid': {
        'events': 'event14',
        'prop19': 'Multiple Errors', // { cb: 'formValidStatus' }
        'prop11': { cb: 's', property: 'channel', suffix: ':sign up' },

        'prop35': 'Sign Up',
        'prop36': 'Submit Error'
      }
    },
    '#webform-client-form-607': {
      'sub_id': 'form-607',
      'valid': {
        'events': 'event6,event12',
        'prop11': { cb: 's', property: 'channel', suffix: ':unsubscribe' },
        'prop35': 'Unsubscribe',
        'prop36': 'Submit'
      },
      'invalid': {
        'events': 'event14',
        'prop11': { cb: 's', property: 'channel', suffix: ':unsubscribe' },
        'prop19': 'Multiple Errors',
        'prop35': 'Unsubscribe',
        'prop36': 'Submit Error'
      }
    }
  });

  omniture.addRules('ajaxComplete', {
    '#views-exposed-form-articles-search-articles': {
      'events': 'event1',
      'prop4': { cb: 'getValue', field: 'input[name="field_search_value"]' },
      'prop5': { cb: 'getText', parent: '.view-articles', find: '.view-header span.total', default: '0' },
      'prop35': 'Search',
      'prop36': 'Internal Search'
    }
  });

  omniture.addRules('slider', {
    'joints': {
      'prop35': { cb: 'extra', property: 'pageName' },
      'prop36': { cb: 'getText', find: 'h2' },
      'prop38': { cb: 'counterText', parent: false }
    }
  });
})(jQuery)
