(function($) {
  var jsRate = function (type, context) {
    var types = {
      'like': {
        id: 1,
        use_js: true
      },
      'pledge': {
        id: 2,
        use_js: false
      }
    }
    , rate_type = type in types ? type : 'like';

    this.nids     = [];
    this.context  = context;
    this.type     = rate_type;
    this.type_id  = types[rate_type].id;
    this.use_js   = types[rate_type].use_js;
  }

  jsRate.prototype.grab = function() {
    var self = this;

    $('div.rate-widget-' + this.type_id + ' > .rate', this.context).each(function() {
      self.nids.push($(this).data('nid'));
    });

    return self;
  }

  jsRate.prototype.send = function() {
    var self = this
      , data;

    if (this.nids.length) {
      data = { nids: this.nids, r: Math.random() };
      if (Drupal.settings.ra_js.is_node) {
         data.is_node = 1;
      }

      $.ajax({
        url: Drupal.settings.basePath + (this.use_js ? 'js/' : '') + 'ra_js/' + this.type,
        type: 'GET',
        dataType: 'json',
        data: data,
        success: function(data) {
          self.nids = [];
          self.updateLikes(data);
        }
      });
    }
  };

  jsRate.prototype.updateLikes = function(data) {
    var self = this;
    $('div.rate-widget-' + this.type_id + ' > .rate', this.context).each(function() {
      var nid = $(this).data('nid')
        , button = $(this).find('.rate-button');

      if (nid in data) {
        if (typeof data[nid] === 'object') {
          $(this).find('.rate-count').text(data[nid].count);
          if (Boolean(data[nid].user_vote)) {
            $(this).addClass('user-voted-wrapper');
            button.addClass('user-voted');
          }

          if (Boolean(data[nid].user_vote)) {
            button.text(Drupal.t('Liked'));
          }
          else {
            button.text(Drupal.formatPlural(data[nid].count, 'Like', 'Likes'));
          }
        }
        else {
          $(this).parent().replaceWith(data[nid]);

          Drupal.behaviors.rate.attach(self.context);
          RA.callbacks.callPledgeAssign();
        }
      }
    });
  };

  jsRate.prototype.init = function() {
    this.grab().send();
  };

  Drupal.behaviors.cachedRate = {
    attach: function (context, settings) {
      if (!$(context).children(":first").hasClass('rate-widget')) {
        new jsRate('like', context).init();
        new jsRate('pledge', context).init();
      }
    }
  };
})(jQuery)
