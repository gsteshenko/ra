<?php
/**
 * @file ra_apachesolr_views.views.inc
 * TODO: Enter file description here.
 */

/**
 * Implements hook_views_query_alter().
 */
function ra_apachesolr_views_views_query_alter(&$view, &$query) {
  // Checking for display allowed to be processed through solr.
  if (_ra_apachesolr_views_solr_display($view->current_display)) {
    $condition_groups = array();
    // Checking all conditions.
    foreach ($query->where as $cg_key => &$condition_group) {
      $condition_groups[] = $condition_group;

      // Unsetting all conditions.
      unset($query->where[$cg_key]);
    }

    if ($condition_groups) {
      $nids = _ra_apachesolr_views_solr_get_nids($view, $condition_groups);

      if ($nids) {
        $query->add_where(NULL, 'node.nid', $nids, 'IN');
      }
    }
  }
}
