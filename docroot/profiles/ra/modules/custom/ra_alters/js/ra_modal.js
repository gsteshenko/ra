(function ($) {
  Drupal.behaviors.initModalFormsContact = {
    attach: function (context, settings) {
      $('a.ext:not(.continue):not(.social)').filter(function() {
          var re = /gene\.com|genentechratreatments\.com/i;
          return !re.test(this.href);
        })
        .addClass('modal ctools-modal-modal-popup-interstitial')
        .each(function() {
          $(this).attr('href', '/modal/nojs/interstitial/leaving_site?link=' + encodeURIComponent(this.href));
        });

      $('a:not(.continue)').filter(function() {
          var re = /genentechratreatments\.com/i;
          return re.test(this.href);
        })
        .addClass('modal ctools-modal-modal-popup-interstitial')
        .each(function() {
          $(this).attr('href', '/modal/nojs/interstitial/prescription?link=' + encodeURIComponent(this.href));
        });

      $("a.modal[href*='/printmail'], a.modal[href*='?q=printmail']", context).once('init-modal-forms-contact', function () {
        var pattern = /printmail\/[0-9]+/;
        if (pattern.test(this.href)) {
          this.href = this.href.replace(pattern, 'modal_forms/nojs/' + this.href.match(pattern));
        }
      }).addClass('ctools-use-modal');

      $("a.modal").addClass('ctools-use-modal').click(function() {
        var choice = Drupal.CTools.Modal.getSettings(this);

        if (Drupal.settings[choice].modalSize.type == 'fixed'
          && $(window).width() <= Drupal.settings[choice].modalSize.width) {
          Drupal.settings[choice].modalSize = {
            type: 'scale',
            width: 0.99,
            height: $(window).width() / $(window).height()
          }
        }
      });
    }
  };
})(jQuery);
