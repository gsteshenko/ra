<?php
/**
 * @file ra_alters.inline.php.inc
 * TODO: Enter file description here.
 */


/**
 * Custom perm callback for cookbook placeholder.
 *
 * @see Articles view, Expert Cookbook Placeholder display.
 */
function _ra_alters_cookbook_placeholder_enabled() {
  $node = menu_get_object();

  // Check if user is Melinda (:nid = 390) to show Coockbook placeholder.
  if ($node->nid && $node->nid != 390) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Check if user visited site first time.
 *
 * @see Header Menus mini panel, Sign Up pane.
 */
function _ra_alters_check_anon_first_visit() {
  return user_is_anonymous()
    && (!isset($_COOKIE['Drupal_visitor_first_visited'])
      || $_COOKIE['Drupal_visitor_first_visited']);
}

/**
 * Get HP inspiration social links.
 *
 * @see Inspirations view, Inspiration of the Day, Inspiration Popup displays.
 */
function _ra_alters_hp_inspiration_links($results, $type = 'inspiration_links') {
  $inspiration = reset($results);
  return theme('ra_alters_' . $type
    , array('node' => $inspiration, 'render' => TRUE));
}

/**
 * Make computed field for article/recipe search.
 *
 * @see artcile/recipe content types field_search field settings.
 */
function _ra_alters_computed_search_field($entity, $entity_type) {
  if (isset($entity->field_article_body)) {
    $article_search = array_pop(field_get_items($entity_type, $entity, 'field_article_body'));
  }
  if (isset($entity->field_expert)) {
    $expert = array_pop(field_get_items($entity_type, $entity, 'field_expert'));
    $expert_node = node_load($expert['nid']);
    $expert_search = $expert_node->title;
  }
  if (isset($entity->field_recipe_main_ingredient)) {
    $main_ingredient = array_pop(field_get_items($entity_type, $entity, 'field_recipe_main_ingredient'));
    $main_ingredient_term =  taxonomy_term_load($main_ingredient['tid']);
    $recipe_search_main_ingredient = $main_ingredient_term->name;
  }
  if (isset($entity->field_recipe_ingredients)) {
    $recipe_search_ingredients = array_pop(field_get_items($entity_type, $entity, 'field_recipe_ingredients'));
  }
  if (isset($entity->field_recipe_directions)) {
    $recipe_search_directions = array_pop(field_get_items($entity_type, $entity, 'field_recipe_directions'));
  }

  return $entity->title . ' ' . strip_tags($article_search['value']) .
    ' ' . $expert_search .
    ' ' . $recipe_search_main_ingredient .
    ' ' . strip_tags($recipe_search_ingredients['value'])  .
    ' ' . strip_tags($recipe_search_directions['value']);
}
