<?php
/**
 * @file ra_alters.admin.inc
 * TODO: Enter file description here.
 */


/**
 * RA settings form.
 */
function ra_alters_global_settings($form, &$form_state) {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['general']['ra_general_overriden_links'] = array(
    '#title' => t('Overriden links'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ra_general_overriden_links', ''),
    '#required' => FALSE,
  );

  $form['fonts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Font Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['fonts']['ra_fonts_external_code'] = array(
    '#title' => t('External Fonts code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ra_fonts_external_code', ''),
    '#required' => FALSE,
  );

  $interstitial_popups = array(
    'leaving_site' => 'Leaving site popup',
    'prescription' => 'Prescription treatment options popup',
  );

  $form['interstitial'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interstitial Page Settings'),
    '#collapsible' => TRUE,
  );

  foreach ($interstitial_popups as $idx => $title) {
    $form['interstitial']['interstitial_' . $idx] = array(
      '#title' => t($title),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['interstitial']['interstitial_' . $idx]['ra_interstitial_' . $idx . '_title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ra_interstitial_' . $idx . '_title', ''),
    );

    $form['interstitial']['interstitial_' . $idx]['ra_interstitial_' . $idx . '_desc'] = array(
      '#title' => t('Body'),
      '#type' => 'textarea',
      '#default_value' => variable_get('ra_interstitial_' . $idx . '_desc', ''),
    );
  }

  $form['pledge'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pledge Settings'),
    '#collapsible' => TRUE,
  );

  $form['pledge']['messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pledge Messages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $messages = array(
    'ra_pledge_message_vote' => t('Pledge vote status message'),
    'ra_pledge_message_thanks' => t('Pledge thanks status message'),
    'ra_pledge_message_vote_node' => t('Node pledge vote status message'),
    'ra_pledge_message_thanks_node' => t('Node pledge thanks status message'),
  );

  foreach ($messages as $key => $title) {
    foreach (array(FALSE, TRUE) as $is_plural) {
      $key = $key . ($is_plural ? '_plural' : '');

      $form['pledge']['messages'][$key] = array(
        '#title' => t($title . ($is_plural ? ' (plural)' : ' (singular)')),
        '#type' => 'textarea',
        '#default_value' => variable_get($key, ''),
        '#required' => TRUE,
      );
    }
  }

  return system_settings_form($form);
}
