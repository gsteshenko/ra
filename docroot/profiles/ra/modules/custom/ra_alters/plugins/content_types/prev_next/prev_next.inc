<?php
/**
 * @author Matt Korostoff <matt@blinkreaction.com>
 *
 * This generates a content pane with a "next/previous" style navigation for
 * whichever node page it's present on.  The nodes will be sorted by 
 * "Published date."  If the pane is included on anything other than a node
 * page, it does nothing.  If the node does not have a "published date" this
 * does nothing.
 */

$plugin = array(
  'title' => t('Prev/Next'),
  'description' => t('Previous and next navigation'),
  'render callback' => 'ra_alters_prev_next_content_type_render',
  'defaults' => array(),
  'edit form' => 'ra_alters_prev_next_content_type_edit_form',
  'category' => array(t('Custom blocks'), 0),
);


/**
 * Render function.
 */
function ra_alters_prev_next_content_type_render($subtype, $conf) {
  $block = new stdClass();
  $node = menu_get_object();

  /**
   * Fetch all the nodes for the current content type.  That is to say, if this
   * executes on an article page, we will return a list of all articles. We 
   * need an ordered list, sorted by date.
   */
  if ($node && $node->type && $node->field_publish_date) {
    $query_options = array(
      ':type' => $node->type,
      ':status' => '1',
    );

    /**
     * This query will produce a list of nids ordered by published date.  We 
     * load these nodes into an array, preserving the order.  Then we can then
     * manipulate and analyze this array find the ordinal position of any given
     * node, as well as the ordinal position of the next and previous nodes.
     */
    $result = db_query('SELECT n.nid FROM {node} n '
      . 'INNER JOIN {field_data_field_publish_date} d ON d.entity_id = n.nid '
      . 'WHERE n.type = :type AND n.status = :status '
      . 'ORDER BY d.field_publish_date_value '
      . 'LIMIT 0,1000', $query_options)->fetchAll();

     //This is the total number of nodes.  E.g. "3 of 20" in which "20 == count"
    $count = count($result);
    
    /**
     * Iterate through the array, until you find the current node's NID.  The
     * array is numerically keyed, so for instance if ($result[2] = 123) then we 
     * know that node 123 is chronologically 3rd overall.
     */
    while (current($result)->nid != $node->nid) next($result);
    $position = key($result)+1;

    /** 
     * Find the next node and generate a link pointing at it. Default to a 
     * nonlink disabled state.
     */
    $next_button = '<span class="next-disabled">Next</span>';
    if ($position < $count) {
      $next_nid = $result[$position]->nid;
      $next_button = l('Next', 'node/' . $next_nid);
    }

     /** 
     * Find the previous node and generate a link pointing at it. Default to a 
     * nonlink disabled state.
     */
    $previous_button = '<span class="previous-disabled">Prev</span>';
    if ($position > 0) {
      $prev_nid = $result[$position]->nid;
      $prev_button = l('Prev', 'node/' . $prev_nid);
    }

    //And now the action.  This generates the actual html output.
    $block->content = $prev_button . $position . ' of ' . $count . $next_button;
  }
  return $block;
}

/**
 * Edit form definition function.
 */
function ra_alters_prev_next_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  return $form;
}
