<?php

define('RA_ALTERS_WELCOME_TITLE', 'Welcome to RheumatoidArthritis.com');
define('RA_ALTERS_WELCOME_DESC', 'Start expanding your RA world. Everyone on this site has RA or works in the RA world. They\'ll share their expert advice with you through articles, tools, and videos.');

$plugin = array(
  'title' => t('Welcome'),
  'description' => t('Welcome block.'),
  'single' => TRUE,
  'content_types' => array('welcome'),
  'render callback' => 'ra_alters_welcome_content_type_render',
  'defaults' => array(),
  'edit form' => 'ra_alters_welcome_content_type_edit_form',
  'category' => array(t('Custom blocks'), 0),
);


/**
 * Render function.
 */
function ra_alters_welcome_content_type_render($subtype, $conf) {
  $block = new stdClass();
  $block->title = '';

  $classes = array('welcome-block');

  $date_day = date('l');
  $date_month = date('F d');
  $title = t($conf['title']);
  $description = t($conf['description']);

  if ($conf['classes']) {
    $classes[] = $conf['classes'];
  }

  $class = implode(' ', $classes);
  $block->content = <<<HTML
<div class="$class">
  <div class="welcome-date">$date_day <span>$date_month</span></div>
  <div class="welcome-title">$title</div>
  <div class="welcome-description">$description</div>
</div>
HTML;

  return $block;
}


/**
 * Edit form definition function.
 */
function ra_alters_welcome_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $default = isset($conf['title']) ? $conf['title'] : RA_ALTERS_WELCOME_TITLE;
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $default,
  );

  $default = isset($conf['description']) ? $conf['description'] : RA_ALTERS_WELCOME_DESC;
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#default_value' => $default,
  );

  $form['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra Classes'),
    '#default_value' => $conf['classes'],
  );

  return $form;
}


/**
 * Form submit function.
 */
function ra_alters_welcome_content_type_edit_form_submit($form, &$form_state) {
  $fields = array(
    'title',
    'description',
    'classes',
  );

  foreach ($fields as $key) {
    $form_state['conf'][$key] = check_plain($form_state['values'][$key]);
  }
}
