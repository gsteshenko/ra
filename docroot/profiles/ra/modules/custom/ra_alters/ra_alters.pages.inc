<?php
/**
 * @file ra_alters.pages.inc
 * TODO: Enter file description here.
 */


/**
 * A modal inspiration list callback.
 */
function ra_alters_inspiration_list($js = NULL) {
  $html = views_embed_view('inspirations', 'inspiration_popup');

  // Fall back if $js is not set.
  if (!$js) {
    return $html;
  }

  ctools_include('modal');
  ctools_include('ajax');

  $commands = array();
  $commands[] = ctools_modal_command_display($title, $html);
  print ajax_render($commands);
}


/**
 * A modal inspiration list callback.
 */
function ra_alters_interstitial_popup($js = NULL, $type = 'leaving_site') {
  $continue_query = array();

  $html = '<div class="modal-content-inner"><div class="modal-title">'
    . variable_get('ra_interstitial_' . $type . '_title', '')
    . '</div>';

  // If popup type is prescription then we pass cid and moc pars to the
  // redirection URL.
  if ('prescription' == $type) {
    $continue_query = array(
      'cid' => 'raf_WE_MIUARAWB0102',
      'moc' => 'MIUARAWB0102',
    );
  }

  $html .= variable_get('ra_interstitial_' . $type . '_desc', '')
    . '<div class="interstitial-buttons">'
    . l(t('Continue'), $_GET['link'], array('attributes' => array('class' => 'continue', 'target' => '_blank'), 'query' => $continue_query))
    . l(t('Back'), '<front>', array('attributes' => array('class' => 'popup-close')))
    . '</div></div>';

  // Fall back if $js is not set.
  if (!$js) {
    return $html;
  }

  ctools_include('modal');
  ctools_include('ajax');

  $commands = array();
  $commands[] = ctools_modal_command_display('', $html);
  print ajax_render($commands);
}

/**
 * A modal sendmail callback.
 */
function ra_alters_printemail_node($js = NULL, $node) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('ra_alters_send_email', $node);
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Send this @type to someone!', array('@type' => $node->type)),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        (object) $node,
      ),
    ),
  );

  $output = ctools_modal_form_wrapper('ra_alters_send_email', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();

    ctools_add_js('ajax-responder');
    if ($form_state['values']['redirect']) {
      $output[] = ctools_ajax_command_redirect('updates-registration');
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }

  print ajax_render($output);
}

/**
 * Send mail form.
 */
function ra_alters_send_email($form, $form_state, $node) {
  $form = array();

  $path = 'node/' . $node->nid;
  $path_title = _print_get_title($path);

  $form['path'] = array('#type' => 'value', '#value' => $path);
  $form['title'] = array('#type' => 'value', '#value' => $path_title);

  $form['txt_to_addrs'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#size' => 62,
    '#required' => TRUE,
    '#placeholder' => t('Recipient\'s e-mail address'),
  );

  $form['fld_from_addr'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#size' => 62,
    '#required' => TRUE,
    '#placeholder' => t('Your e-mail address'),
  );

  $form['redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check this box to provide additional information and receive more from @site_url.com'
                , array('@site_url' => variable_get('site_name', ''))),
  );

  $form['btn_submit'] = array(
    '#name' => 'submit',
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Send mail form validate.
 */
function ra_alters_send_email_validate($form, &$form_state) {
  module_load_include('inc', 'print_mail', 'print_mail');
  $values =& $form_state['values'];

  $values += array(
    'fld_from_name' => variable_get('site_mail', ''),
    'fld_subject'   => $values['title'],
    'txt_message'   => '',
  );

  print_mail_form_validate($form, $form_state);
}

/**
 * Send mail form submit.
 */
function ra_alters_send_email_submit($form, &$form_state) {
  if (!array_key_exists('cancel', $form_state['values'])) {
    $cid = isset($form_state['values']['cid']) ? $form_state['values']['cid'] : NULL;
    $print_mail_text_message = filter_xss_admin(variable_get('print_mail_text_message', t('Message from sender')));
    $sender_message = $print_mail_text_message . ':<br /><br /><em>' . nl2br(check_plain($form_state['values']['txt_message'])) . '</em>';

    $print = print_controller($form_state['values']['path'], $form_state['values']['query'], $cid, PRINT_MAIL_FORMAT, $form_state['values']['chk_teaser'], $sender_message);

    if ($print !== FALSE) {
      $print_mail_send_option_default = variable_get('print_mail_send_option_default', PRINT_MAIL_SEND_OPTION_DEFAULT);

      $params = array();
      $params['subject'] = $form_state['values']['fld_subject'];
      $params['message'] = '';
      $params['link'] = $print['url'];
      $params['title'] = $form_state['values']['title'];

      // If a name is provided, make From: in the format Common Name <address>
      if (!empty($form_state['values']['fld_from_name'])) {
        $from = '"' . mime_header_encode($form_state['values']['fld_from_name']) . '" <' . $form_state['values']['fld_from_addr'] . '>';
      }
      else {
        $from = $form_state['values']['fld_from_addr'];
      }

      // If using reply-to, move the From: info to the params array, so that it is passed to hook_mail later
      if (variable_get('print_mail_use_reply_to', PRINT_MAIL_USE_REPLY_TO)) {
        $params['from'] = $from;
        $from = NULL;
      }

      $from_addr = $form_state['values']['fld_from_addr'];

      // Spaces in img URLs must be replaced with %20
      $pattern = '!<(img\s[^>]*?)>!is';
      $print['content'] = preg_replace_callback($pattern, '_print_replace_spaces', $print['content']);

      $node = $print['node'];
      $params['body'] = theme('print', array('print' => $print, 'type' => PRINT_MAIL_FORMAT, 'node' => $node, 'from' => $from_addr));

      // Img elements must be set to absolute
      $pattern = '!<(img\s[^>]*?)>!is';
      $params['body'] = preg_replace_callback($pattern, '_print_rewrite_urls', $params['body']);

      // Convert the a href elements, to make sure no relative links remain
      $pattern = '!<(a\s[^>]*?)>!is';
      $params['body'] = preg_replace_callback($pattern, '_print_rewrite_urls', $params['body']);

      $ok = FALSE;
      $use_job_queue = variable_get('print_mail_job_queue', PRINT_MAIL_JOB_QUEUE_DEFAULT);
      if ($use_job_queue) {
        $queue = DrupalQueue::get('print_mail_send');
      }

      $addresses = explode(', ', $form_state['values']['txt_to_addrs']);
      foreach ($addresses as $to) {
        if ($use_job_queue) {
          // Use job queue to send mails during cron runs
          $queue->createItem(array('module' => 'print_mail', 'key' => $print_mail_send_option_default, 'to' => $to, 'language' => language_default(), 'params' => $params, 'from' => $from));
        }
        else {
          // Send mail immediately using Drupal's mail handler
          $ret = drupal_mail('print_mail', $print_mail_send_option_default, $to, language_default(), $params, $from);
        }
        if ($use_job_queue || $ret['result']) {
          flood_register_event('print_mail');
          $ok = TRUE;
        }
      }
      if ($ok) {
        $query = empty($form_state['values']['query']) ? '' : '?' . rawurldecode(drupal_http_build_query($form_state['values']['query']));
        watchdog('print_mail', '%name [%from] sent %page to [%to]', array('%name' => $form_state['values']['fld_from_name'], '%from' => $form_state['values']['fld_from_addr'], '%page' => $form_state['values']['path'] . $query, '%to' => $form_state['values']['txt_to_addrs']));
        $site_name = variable_get('site_name', t('us'));
        $print_mail_text_confirmation = variable_get('print_mail_text_confirmation', t('Thank you for spreading the word about !site.'));
        drupal_set_message(check_plain(t($print_mail_text_confirmation, array('!site' => $site_name))));

        $nodepath = drupal_get_normal_path($form_state['values']['path']);
        db_update('print_mail_page_counter')
          ->fields(array(
              'sentcount' => 1,
              'sent_timestamp' => REQUEST_TIME,
          ))
          ->condition('path', $nodepath, '=')
          ->expression('sentcount', 'sentcount + :inc', array(':inc' => count($addresses)))
          ->execute();
      }
    }
  }

  if ($form_state['values']['redirect']) {
    $form_state['redirect'] = 'updates-registration';
  }
  else {
    $form_state['redirect'] = array(preg_replace('!^book/export/html/!', 'node/', $form_state['values']['path']), array('query' => $form_state['values']['query']));
  }
}
