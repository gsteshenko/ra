<?php
/**
 * Place update hooks in this file.
 *
 * Commonly used functions:
 *
 * ra_features_revert($modules, $force = FALSE);
 * Used to revert features (i.e. update the database to match code).
 * See definition in ra.install
 *
 */

/**
 * Enables views_infinite_scroll module as pager alternative.
 * Reverts ra_article, ra_homepage features - updated homepage panel, added new
 *   article components based on featured articles nodequeue.
 */
function role_out_a_update_7001() {
  $enable = array(
    'views_infinite_scroll',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_homepage',
  );
  ra_features_revert($revert);
}

/**
 * Enables views_infinite_scroll module as pager alternative.
 * Reverts ra_article, ra_layout, ra_recipe features - created search HP popup,
 *   block, updated header monipanel block, updated recipe ingredient field.
 */
function role_out_a_update_7002() {
  $enable = array(
    'ra_ctools_context',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_layout',
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_topic, ra_settings features - topic page, like updates.
 */
function role_out_a_update_7003() {
  $revert = array(
    'ra_article',
    'ra_topic',
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_recipe - added social links, email and like.
 */
function role_out_a_update_7004() {
  $enable = array(
    'print',
    'print_mail',
    'social_share',
    'modal_forms',
    'ra_alters',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_recipe',
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert re_recipe, ra_topic features - recipe menu, search block.
 */
function role_out_a_update_7005() {
  $enable = array(
    'menu_views',
  );
  module_enable($enable);

  $revert = array(
    're_recipe',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert re_expert, ra_topic features - topic expert, topic poll blocks.
 */
function role_out_a_update_7006() {
  $enable = array(
    'poll',
    'pm_existing_pages',
  );
  module_enable($enable);

  $revert = array(
    're_expert',
    're_recipe',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert re_recipe, ra_topic features - new taxonomy icon fields.
 */
function role_out_a_update_7007() {
  $revert = array(
    're_recipe',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert re_expert, ra_article features - field settings updates.
 */
function role_out_a_update_7008() {
  // Generating some dummy content
  if (module_exists('devel_generate')) {
    module_load_include('inc', 'devel_generate', 'devel_generate');

    // Devel content form state
    $form_state = array(
      'values' => array(
        'num_nodes' => 10,
        'title_length' => 3,
        'node_types' => array(
          'article' => 'article',
          'recipe' => 'recipe',
          'expert' => 'expert',
        ),
      )
    );
    // Delete content before changing expert, article structure
    devel_generate_content_kill($form_state['values']);

    // Delete body field instance from article bundle
    $article_body = field_info_instance('node', 'body', 'article');
    field_delete_instance($article_body);

    $revert = array(
      're_expert',
      're_recipe',
      'ra_article',
      'ra_topic',
    );
    ra_features_revert($revert);

    $form_state['values']['node_types'] = array('expert' => 'expert');
    // Generating new content - experts
    devel_generate_content($form_state);

    $form_state['values']['num_nodes'] = 50;
    $form_state['values']['node_types'] = array(
      'article' => 'article',
      'recipe' => 'recipe',
    );

    // Generating new content - recipes, articles
    devel_generate_content($form_state);
  }
}

/**
 * Enable image_allow_insecure_derivatives module.
 */
function role_out_a_update_7009() {
  $enable = array(
    'image_allow_insecure_derivatives',
  );
  module_enable($enable);
}

/**
 * Enable apachesolr support, revert recipe, article, topic features.
 */
function role_out_a_update_7010() {
  $enable = array(
    'acquia_agent',
    'acquia_search',
    'acquia_spi',
    'apachesolr',
    'apachesolr_search',
    'apachesolr_autocomplete',
    'ra_apachesolr_views',
  );
  module_enable($enable);

  $revert = array(
    're_expert',
    're_recipe',
    'ra_article',
    'ra_topic',
  );
  ra_features_revert($revert);

  variable_set('acquia_identifier', 'LQVX-20780');
  variable_set('acquia_key', '5c0dd5d3b885f4b66d5d9f0aba643f63');
}

/**
 * Enable extlink module, revert ra_settings - breadcrumbs, extlink settings.
 */
function role_out_a_update_7011() {
  $enable = array(
    'extlink',
  );
  module_enable($enable);

  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_recipe - pager updates, recipes header.
 */
function role_out_a_update_7012() {
  $revert = array(
    'ra_article',
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Enable pollfield, ra_poll modules, revert ra_topic - added topic polls.
 */
function role_out_a_update_7013() {
  $enable = array(
    'pollfield',
    'ra_poll',
  );
  module_enable($enable);

  $revert = array(
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_topic - added signup to the topics.
 */
function role_out_a_update_7014() {
  $revert = array(
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Enable apachesolr_multisitesearch module.
 */
function role_out_a_update_7015() {
  $enable = array(
    'apachesolr_multisitesearch',
  );
  module_enable($enable);

  variable_del('apachesolr_site_hash');
  $env_id = variable_get('apachesolr_default_environment', 'solr');
  apachesolr_environment_variable_set($env_id, 'multisitesearch', 1);
}

/**
 * Revert ra_article, ra_expert, ra_settings - expert like, added recipe filter.
 */
function role_out_a_update_7016() {
  $revert = array(
    'ra_article',
    'ra_expert',
    'ra_poll',
    'ra_recipe',
    'ra_settings',
  );
  ra_features_revert($revert);

  // Delete body field instance from mpoll bundle
  $poll_topic = field_info_instance('node', 'field_topic', 'mpoll');
  field_delete_instance($poll_topic);
}

/**
 * Revert ra_article - added search close button.
 */
function role_out_a_update_7017() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert - changed title to Full Name.
 */
function role_out_a_update_7018() {
  $revert = array(
    'ra_expert',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_recipe - added print recipe in similar recipes footer.
 */
function role_out_a_update_7019() {
  $revert = array(
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_recipe - added print recipe pane.
 */
function role_out_a_update_7020() {
  $revert = array(
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_recipe - required image field.
 */
function role_out_a_update_7021() {
  $revert = array(
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Enable ra_pledge - new pledge CT, reverts ra_homepage, ra_settings.
 */
function role_out_a_update_7022() {
  $enable = array(
    'ra_pledge',
  );
  module_enable($enable);

  $revert = array(
    'ra_homepage',
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Enable views_php, ra_inspiration - new inspiration CT, reverts ra_homepage.
 */
function role_out_a_update_7023() {
  $enable = array(
    'views_php',
    'ra_inspiration',
  );
  module_enable($enable);

  $revert = array(
    'ra_homepage',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_layout - added toggle button.
 */
function role_out_a_update_7024() {
  $revert = array(
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_layout - footer sign-up-info class.
 */
function role_out_a_update_7025() {
  $revert = array(
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_recipe, ra_settings, ra_topic.
 */
function role_out_a_update_7026() {
  $revert = array(
    'ra_article',
    'ra_recipe',
    'ra_settings',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_settings - added article type, teaser fields.
 */
function role_out_a_update_7027() {
  $enable = array(
    'conditional_fields',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_settings',
  );
  ra_features_revert($revert);

  // updated all articles with default article type
  $articles = db_select('node', 'n')
    ->fields('n')
    ->condition('type', 'article')
    ->execute();

  foreach ($articles as $article) {
    $node = node_load($article->nid);
    $node->field_article_type[LANGUAGE_NONE][0]['value'] = 'default';
    node_save($node);
  }
}

/**
 * Revert ra_settings, ra_homepage.
 */
function role_out_a_update_7028() {
  $revert = array(
    'ra_settings',
    'ra_homepage',
  );
  ra_features_revert($revert);
}

/**
 * Enable ra_webform module.
 */
function role_out_a_update_7029() {
  $enable = array(
    'ra_webform',
  );
  module_enable($enable);
}

/**
 * Revert ra_homepage.
 */
function role_out_a_update_7030() {
  $revert = array(
    'ra_homepage',
  );
  ra_features_revert($revert);
}

/**
 * Enable ra_webform module.
 */
function role_out_a_update_7031() {
  $enable = array(
    'ra_webform',
  );
  module_enable($enable);

  $revert = array(
    'ra_recipe',
    'ra_expert',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7032() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7033() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert.
 */
function role_out_a_update_7034() {
  $revert = array(
    'ra_expert',
  );
  ra_features_revert($revert);
}

/**
 * Enable ra_workflow, revert ra_layout, ra_recipe, ra_webfrom.
 */
function role_out_a_update_7035() {
  $enable = array(
    'ra_workflow',
  );
  module_enable($enable);

  $revert = array(
    'ra_layout',
    'ra_recipe',
    'ra_webfrom',
  );
  ra_features_revert($revert);
}

/**
 * Enable disable_messages, revert ra_layout, ra_settings, ra_article.
 */
function role_out_a_update_7036() {
  $enable = array(
    'disable_messages',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_layout',
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_homepage, ra_topic.
 */
function role_out_a_update_7037() {
  $revert = array(
    'ra_article',
    'ra_homepage',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert, ra_layout.
 */
function role_out_a_update_7038() {
  $revert = array(
    'ra_expert',
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_pledge, ra_poll, ra_webform.
 */
function role_out_a_update_7039() {
  $revert = array(
    'ra_article',
    'ra_pledge',
    'ra_poll',
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_webform.
 */
function role_out_a_update_7040() {
  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_webform.
 */
function role_out_a_update_7041() {
  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Enable advagg, advagg_css_compress.
 */
function role_out_a_update_7042() {
  $enable = array(
    'advagg',
    'advagg_css_compress',
  );
  module_enable($enable);
}

/**
 * Disable advagg, advagg_css_compress, changed bigscreen_media_query.
 */
function role_out_a_update_7043() {
  $disable = array(
    'advagg_css_compress',
    'advagg',
  );
  module_disable($disable);
  drupal_uninstall_modules($disable);

  //Get the existing theme settings array.
  $theme_settings=variable_get('theme_ra_theme_settings');

  //Update some values
  $theme_settings["bigscreen_media_query"] = 'all';

  //save the new values
  variable_set('theme_ra_theme_settings', $theme_settings);
}

/**
 * Enable navigation404.
 */
function role_out_a_update_7044() {
  $enable = array(
    'navigation404',
  );
  module_enable($enable);
}

/**
 * Revert ra_article, ra_layout.
 */
function role_out_a_update_7045() {
  $revert = array(
    'ra_article',
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Updated field_expert_raconnection field with max_length to 128.
 */
function role_out_a_update_7046() {
  db_query('ALTER TABLE `field_data_field_expert_raconnection`
    CHANGE `field_expert_raconnection_value` `field_expert_raconnection_value` VARCHAR( 128 )
    CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');

  db_query('ALTER TABLE `field_revision_field_expert_raconnection`
    CHANGE `field_expert_raconnection_value` `field_expert_raconnection_value` VARCHAR( 128 )
    CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');

  $field_settings = db_query('SELECT CAST(`data` AS CHAR(10000) CHARACTER SET utf8)
    FROM `field_config` WHERE field_name = :field;', array(':field' => 'field_expert_raconnection'))->fetchField();
  $field_settings = str_replace('"max_length";s:2:"64";', '"max_length";s:3:"128";', $field_settings);

  db_query('UPDATE `field_config`
    SET data = :field
    WHERE `field_name` = \'field_expert_raconnection\'', array(':field' => $field_settings));
}

/**
 * Revert ra_pledge.
 */
function role_out_a_update_7047() {
  $revert = array(
    'ra_pledge',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_pledge.
 */
function role_out_a_update_7048() {
  $revert = array(
    'ra_pledge',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_topic.
 */
function role_out_a_update_7049() {
  $revert = array(
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert, ra_inspiration.
 */
function role_out_a_update_7050() {
  $revert = array(
    'ra_article',
    'ra_expert',
    'ra_inspiration',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7051() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7052() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_topic.
 */
function role_out_a_update_7053() {
  $revert = array(
    'ra_article',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_inspiration.
 */
function role_out_a_update_7054() {
  $revert = array(
    'ra_inspiration',
  );
  ra_features_revert($revert);
}

/**
 * Disable poll module.
 */
function role_out_a_update_7055() {
  $disable = array(
    'poll',
  );
  module_disable($disable);

  $uninstall = array(
    'poll',
  );
  drupal_uninstall_modules($uninstall);

  node_type_delete('poll');
  node_types_rebuild();
  menu_rebuild();
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7056() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7057() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_topic.
 */
function role_out_a_update_7058() {
  $revert = array(
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7059() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7060() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Enable webform_addmore, revert ra_webform.
 */
function role_out_a_update_7061() {
  $enable = array(
    'webform_addmore',
  );
  module_enable($enable);

  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);

  variable_set('webform_allowed_tags'
    , array('a', 'em', 'strong', 'code', 'img', 'ul', 'ol', 'li', 'i'));
}

/**
 * Revert webform_addmore.
 */
function role_out_a_update_7062() {
  $revert = array(
    'webform_addmore',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7063() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_webform.
 */
function role_out_a_update_7064() {
  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7065() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_webform.
 */
function role_out_a_update_7066() {
  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings, ra_webform.
 */
function role_out_a_update_7067() {
  $revert = array(
    'ra_settings',
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_webform.
 */
function role_out_a_update_7068() {
  $revert = array(
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7069() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_layout.
 */
function role_out_a_update_7070() {
  $revert = array(
    'ra_article',
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_recipe, ra_webform.
 */
function role_out_a_update_7071() {
  $revert = array(
    'ra_recipe',
    'ra_webform',
  );
  ra_features_revert($revert);
}

/**
 * Enable image_style_quality, resp_img. Revert ra_article, ra_settings.
 */
function role_out_a_update_7072() {
  $enable = array(
    'image_style_quality',
    'resp_img',
  );
  module_enable($enable);

  $revert = array(
    'ra_article',
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_expert, ra_recipe.
 */
function role_out_a_update_7073() {
  $revert = array(
    'ra_article',
    'ra_expert',
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7074() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_layout.
 */
function role_out_a_update_7075() {
  $revert = array(
    'ra_layout',
  );
  ra_features_revert($revert);
}

/**
 * Enable ra_omniture. Revert ra_settings.
 */
function role_out_a_update_7076() {
  $enable = array(
    'ra_omniture',
  );
  module_enable($enable);

  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7077() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7078() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_settings.
 */
function role_out_a_update_7079() {
  $revert = array(
    'ra_settings',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7080() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Delete field_article_summary field.
 */
function role_out_a_update_7081() {
  field_delete_field('field_article_summary');
}

/**
 * Revert ra_article.
 */
function role_out_a_update_7082() {
  $revert = array(
    'ra_article',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_article, ra_recipe.
 */
function role_out_a_update_7083() {
  $revert = array(
    'ra_article',
    'ra_recipe',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert.
 */
function role_out_a_update_7084() {
  $revert = array(
    'ra_expert',
  );
  ra_features_revert($revert);
}

/**
 * Enable js, ra_js modules.
 */
function role_out_a_update_7085() {
  $enable = array(
    'js',
    'ra_js',
  );
  module_enable($enable);
}

/**
 * Revert ra_expert, ra_recipe, ra_topic.
 */
function role_out_a_update_7086() {
  $revert = array(
    'ra_expert',
    'ra_recipe',
    'ra_topic',
  );
  ra_features_revert($revert);
}

/**
 * Revert ra_expert, ra_article.
 */
function role_out_a_update_7087() {
  $revert = array(
    'ra_expert',
    'ra_article',
  );
  ra_features_revert($revert);
}
