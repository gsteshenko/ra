<?php

/**
 * @file
 * Handles Advanced Aggregation installation and upgrade tasks.
 */

/**
 * Implements hook_disable().
 */
function advagg_disable() {
  // Make sure the advagg_get_root_files_dir() function is available.
  drupal_load('module', 'advagg');

  // Make sure the advagg_flush_all_cache_bins() function is available.
  module_load_include('inc', 'advagg', 'advagg');
  module_load_include('inc', 'advagg', 'advagg.cache');

  // Flush caches.
  advagg_flush_all_cache_bins();
}

/**
 * Implements hook_uninstall().
 */
function advagg_uninstall() {
  // Make sure the advagg_get_root_files_dir() function is available.
  drupal_load('module', 'advagg');

  // Make sure the advagg_remove_all_aggregated_files() function is available.
  module_load_include('inc', 'advagg', 'advagg');
  module_load_include('inc', 'advagg', 'advagg.cache');

  // Remove files.
  advagg_remove_all_aggregated_files();
  // Flush caches.
  advagg_flush_all_cache_bins();

  // Remove variables.
  db_delete('variable')
    ->condition('name', 'advagg%', 'LIKE')
    ->execute();

  // Remove Directories.
  list($css_path, $js_path) = advagg_get_root_files_dir();
  drupal_rmdir($css_path[0]);
  drupal_rmdir($js_path[0]);
}

/**
 * Implements hook_schema().
 */
function advagg_schema() {
  // Create cache tables.
  $schema['cache_advagg_aggregates'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_advagg_aggregates']['description'] = t('Cache table for Advanced CSS/JS Aggregation. Used to keep a cache of the CSS and JS HTML tags.');

  $schema['cache_advagg_info'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_advagg_info']['description'] = t('Cache table for Advanced CSS/JS Aggregation. Used to keep a cache of the db and file info.');

  // Create database tables.
  $schema['advagg_files'] = array(
    'description' => 'Files used in CSS/JS aggregation.',
    'fields' => array(
      'filename' => array(
        'description' => 'Path and filename of the file relative to Drupal webroot.',
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'filename_hash' => array(
        'description' => 'Hash of path and filename. Used to join tables.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'content_hash' => array(
        'description' => 'Hash of the file content. Used to see if the file has changed.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'filetype' => array(
        'description' => 'Filetype.',
        'type' => 'varchar',
        'length' => 8,
        'not null' => TRUE,
        'default' => '',
      ),
      'filesize' => array(
        'description' => 'The file size in bytes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'linecount' => array(
        'description' => 'The number of lines in the file.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'mtime' => array(
        'description' => 'The time the file was last modified.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changes' => array(
        'description' => 'This is incremented every time a file changes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'content_hash' => array('content_hash'),
      'filetype' => array('filetype'),
      'filesize' => array('filesize'),
    ),
    'primary key' => array('filename_hash'),
  );

  $schema['advagg_aggregates'] = array(
    'description' => 'What files are used in what aggregates.',
    'fields' => array(
      'aggregate_filenames_hash' => array(
        'description' => 'Hash of the aggregates list of files. Keep track of what files are in the aggregate.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_hash' => array(
        'description' => 'Hash of path and filename.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'porder' => array(
        'description' => 'Processing order.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'settings' => array(
        'description' => 'Extra data about this file and how it is used in this aggregate.',
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'translatable' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'indexes' => array(
      'porder' => array('porder'),
    ),
    'primary key' => array('aggregate_filenames_hash', 'filename_hash'),
  );

  $schema['advagg_aggregates_versions'] = array(
    'description' => 'What files are used in what aggregates.',
    'fields' => array(
      'aggregate_filenames_hash' => array(
        'description' => 'Hash of the aggregates list of files. Keep track of what files are in the aggregate.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'aggregate_contents_hash' => array(
        'description' => 'Hash of all content_hashes in this aggregate. Simple Version control of the aggregate.',
        'type' => 'varchar',
        'length' => 43,
        'not null' => TRUE,
        'default' => '',
      ),
      'atime' => array(
        'description' => 'Last access time for this version of the aggregate. Updated every 12 hours.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'root' => array(
        'description' => 'If 1 then it is a root aggregate. 0 means not root aggregate.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'root' => array('root'),
      'atime' => array('atime'),
      'root_atime' => array(
        'root',
        'atime',
      ),
    ),
    'primary key' => array('aggregate_filenames_hash', 'aggregate_contents_hash'),
  );

  // Copy the variable table & change a couple of things.
  $schema['advagg_aggregates_hashes'] = drupal_get_schema_unprocessed('system', 'variable');
  $schema['advagg_aggregates_hashes']['fields']['hash'] = $schema['advagg_aggregates_hashes']['fields']['name'];
  $schema['advagg_aggregates_hashes']['fields']['settings'] = $schema['advagg_aggregates_hashes']['fields']['value'];
  unset($schema['advagg_aggregates_hashes']['fields']['name'], $schema['advagg_aggregates_hashes']['fields']['value']);
  $schema['advagg_aggregates_hashes']['fields']['hash']['length'] = 43;
  $schema['advagg_aggregates_hashes']['fields']['hash']['description'] = t('The name of the hash.');
  $schema['advagg_aggregates_hashes']['fields']['settings']['description'] = t('The settings associated with this hash.');
  $schema['advagg_aggregates_hashes']['description'] = t('Key value pairs created by AdvAgg. Stores settings used at the time that the aggregate was created.');
  $schema['advagg_aggregates_hashes']['primary key'][0] = 'hash';

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function advagg_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time
  $t = get_t();

  // Report Drupal version
  if ($phase == 'runtime') {
    list($css_path, $js_path) = advagg_get_root_files_dir();

    // Make sure directories are writable.
    if (!file_prepare_directory($css_path[0], FILE_CREATE_DIRECTORY)) {
      $requirements['advagg_css_path'] = array(
        'title' => $t('Adv CSS/JS Agg - CSS Path'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('CSS directory is not created or writable'),
        'description' => $t('%path is not setup correctly.', array('%path' => $css_path)),
      );
    }
    if (!file_prepare_directory($js_path[0], FILE_CREATE_DIRECTORY)) {
      $requirements['advagg_js_path'] = array(
        'title' => $t('Adv CSS/JS Agg - JS Path'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('JS directory is not created or writable'),
        'description' => $t('%path is not setup correctly.', array('%path' => $js_path)),
      );
    }

    // Make sure variables are set correctly.
    if (!variable_get('preprocess_css', FALSE) || !variable_get('preprocess_js', FALSE)) {
      $requirements['advagg_core_off'] = array(
        'title' => $t('Adv CSS/JS Agg - Core Variables'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('Cores CSS and/or JS aggregation is disabled.'),
        'description' => $t('"Optimize CSS files" and "Optimize JavaScript files" on the <a href="@performance">performance page</a> should be enabled.', array('@performance' => url('admin/config/development/performance'))),
      );
    }
    if (variable_get('advagg_enabled', ADVAGG_ENABLED) == FALSE) {
      $requirements['advagg_not_on'] = array(
        'title' => $t('Adv CSS/JS Agg - Enabled'),
        'severity' => REQUIREMENT_WARNING,
        'value' => $t('Advanced CSS/JS aggregation is disabled.'),
        'description' => $t('Go to the Advanced CSS/JS aggregation <a href="@settings">settings page</a> and enable it.', array('@settings' => url('admin/settings/advagg'))),
      );
    }

    // Check that the menu router handler is working.
    $item = menu_get_item($css_path[1] . '/test.css');
    if (empty($item['page_callback']) || strpos($item['page_callback'], 'advagg') === FALSE) {
      $item = str_replace('    ', '&nbsp;&nbsp;&nbsp;&nbsp;', nl2br(htmlentities(print_r($item, TRUE))));
      $requirements['advagg_async_generation_menu_issue'] = array(
        'title' => $t('Adv CSS/JS Agg - Async Mode'),
        'severity' => REQUIREMENT_WARNING,
        'value' => $t('Flush your caches'),
        'description' => $t('You need to flush your menu cache. This can be done at the top of the <a href="@performance">performance page</a>. If this does not fix the issue copy this info below when opening up an <a href="http://drupal.org/node/add/project-issue/advagg">issue for advagg</a>: <br /> !info', array(
          '@performance' => url('admin/config/development/performance'),
          '!info' => $item,
        )),
      );
    }
    $item = menu_get_item($js_path[1] . '/test.js');
    if (empty($item['page_callback']) || strpos($item['page_callback'], 'advagg') === FALSE) {
      $item = str_replace('    ', '&nbsp;&nbsp;&nbsp;&nbsp;', nl2br(htmlentities(print_r($item, TRUE))));
      $requirements['advagg_async_generation_menu_issue'] = array(
        'title' => $t('Adv CSS/JS Agg - Async Mode'),
        'severity' => REQUIREMENT_WARNING,
        'value' => $t('Flush your caches'),
        'description' => $t('You need to flush your menu cache. This can be done near the bottom of the <a href="@performance">performance page</a>. If this does not fix the issue copy this info below when opening up an <a href="http://drupal.org/node/add/project-issue/advagg">issue for advagg</a>: <br /> !info', array(
          '@performance' => url('admin/config/development/performance'),
          '!info' => $item,
        )),
      );
    }

    // Make sure http requests will work correctly.
    advagg_install_check_via_http($requirements);

    // If all requirements have been meet, state advagg should be working.
    if (empty($requirements)) {
      $requirements['advagg_ok'] = array(
        'title' => $t('Adv CSS/JS Agg'),
        'severity' => REQUIREMENT_OK,
        'value' => $t('OK'),
        'description' => $t('Advanced CSS/JS Aggregator should be working correctly.'),
      );
    }
  }

  return $requirements;
}

/**
 * Make sure http requests to css/js files work correctly.
 *
 * @param $requirements
 *   array of requirements used in hook_requirements().
 */
function advagg_install_check_via_http(&$requirements) {
  // If other checks have not passed, do not test this.
  if (!empty($requirements)) {
    return;
  }

  // Ensure translations don't break at install time
  $t = get_t();

  // Setup some variables.
  list($css_path, $js_path) = advagg_get_root_files_dir();
  $types = array('css', 'js');

  // Make sure we get an advagg fast 404.
  foreach ($types as $type) {
    if ($type == 'css') {
      $path = $css_path[0];
    }
    elseif ($type == 'js') {
      $path = $js_path[0];
    }
    // Set arguments for drupal_http_request().
    // Make a 404 request to the advagg menu callback.
    $url = file_create_url($path . '/' . $type . REQUEST_TIME . '.' . $type);
    $options = array('timeout' => 10);

    // Send request.
    $request = drupal_http_request($url, $options);

    // Check response. Report an error if
    //  Not a 404.
    //  Headers do not contain "x-advagg".
    //  Body does not contain "advagg_missing_fast404".
    if (   $request->code != 404
        || empty($request->headers['x-advagg'])
        || strpos($request->data, '<!-- advagg_missing_fast404 -->') === FALSE
          ) {
      // Menu callback failed.
      $requirements['advagg_' . $type . '_generation'] = array(
        'title' => $t('Adv CSS/JS Agg - HTTP Request'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('HTTP requests to advagg for ' . $type . ' files are not getting though.'),
        'description' => $t('Raw request info: <pre>@request</pre>', array('@request' => print_r($request, TRUE)))
      );
    }
  }

  // Check gzip encoding.
  foreach ($types as $type) {
    if ($type == 'css') {
      $url_path = $css_path[0];
      $file_path = $css_path[1];
    }
    elseif ($type == 'js') {
      $url_path = $js_path[0];
      $file_path = $js_path[1];
    }

    // Open the advagg directory.
    $handle = opendir($file_path);
    $counter = 0;
    while (FALSE !== ($filename = readdir($handle))) {
      // Skip over . and ..
      if ($filename == '.' || $filename == '..') {
        continue;
      }

      // See if this uri contains .gz near the end of it.
      $pos = strripos($filename, '.gz', 91 + strlen(ADVAGG_SPACE)*3);
      if (!empty($pos)) {
        $len = strlen($filename);
        // If this is a .gz file skip.
        if ($pos == $len-3) {
          continue;
        }
      }

      $gzip_filename = readdir($handle);
      if (variable_get('advagg_gzip', ADVAGG_GZIP)) {
        // Skip if the next file is not a .gz file.
        if (strcmp($filename . '.gz', $gzip_filename) !== 0) {
          continue;
        }
      }
      else {
        // Skip if the next file is a .gz file.
        if (strcmp($filename . '.gz', $gzip_filename) === 0) {
          continue;
        }
      }

      // All checked passed above, break out of loop.
      break;
    }
    // Skip if filename is empty.
    if (empty($filename)) {
      continue;
    }

    // Set arguments for drupal_http_request().
    $url = file_create_url($url_path . '/' . $filename);
    $options = array('timeout' => 10, 'headers' => array('Accept-Encoding' => 'gzip, deflate'));

    // Send request.
    $request = drupal_http_request($url, $options);

    // Check response. Report an error if
    //  Not a 200.
    //  Headers do not contain "content-encoding".
    //  content-encoding is not gzip or deflate.
    if (   $request->code != 200
        || empty($request->headers['content-encoding'])
        || ($request->headers['content-encoding'] != 'gzip' && $request->headers['content-encoding'] != 'deflate')
          ) {
      $config_path = advagg_admin_config_root_path();
      // Gzip failed.
      if (!variable_get('advagg_gzip', ADVAGG_GZIP)) {
        // Recommend that gzip be turned on.
        $requirements['advagg_' . $type . '_gzip'] = array(
          'title' => $t('Adv CSS/JS Agg - gzip'),
          'severity' => REQUIREMENT_WARNING,
          'value' => $t('Gzip is failing for %type files.', array('%type' => $type)),
          'description' => $t('Try enabling on the "Create .gz files" setting on the <a href="@advagg">Advanced CSS/JS Aggregation Configuration page</a>', array(
            '@advagg' => url($config_path . '/advagg'),
            '%type' => $type,
          )),
        );
      }
      else {
        // Recommend servers configuration be adjusted.
        $requirements['advagg_' . $type . '_gzip'] = array(
          'title' => $t('Adv CSS/JS Agg - gzip'),
          'severity' => REQUIREMENT_WARNING,
          'value' => $t('Gzip is failing for %type files.', array('%type' => $type)),
          'description' => $t('The web servers configuration will need to be adjusted. In most cases make sure that the webroots .htaccess file still contains this section "Rules to correctly serve gzip compressed CSS and JS files".', array(
            '@advagg' => url($config_path . '/advagg'),
          )),
        );
      }
    }
  }
}
