<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add setting to webform fieldset components.
 */
function webform_addmore_form_webform_component_edit_form_alter(&$form, $form_state) {
  if ($form['type']['#value'] != 'fieldset') {
    return;
  }
  $settings = variable_get('webform_addmore_' . $form['nid']['#value'], array());
  $component = $settings[$form['cid']['#value']];

  $default = isset($component['enabled']) ? $component['enabled'] : '';
  $form['extra']['addmore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add More fieldset'),
    '#description' => t('A single Add More fieldset will be displayed with an Add More button.'),
    '#default_value' => isset($component['enabled']) ? $component['enabled'] : 0,
    '#weight' => 98,
  );

  $form['extra']['addmore_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('AddMore Settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="extra[addmore]"]' => array('checked' => TRUE),
      ),
    ),
    '#weight' => 99,
  );

  $default = isset($component['button']['add']) ? $component['button']['add'] : '';
  $form['extra']['addmore_settings']['add_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Add Button Text'),
    '#default_value' => $default,
  );

  $default = isset($component['button']['remove']) ? $component['button']['remove'] : '';
  $form['extra']['addmore_settings']['remove_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Remove Button Text'),
    '#default_value' => $default,
  );
}

/**
 * Save webform fieldset setting.
 *
 * @param $component
 *   The Webform component being saved.
 */
function webform_addmore_webform_component_presave(&$component) {
  if ($component['type'] != 'fieldset') {
    return;
  }

  $settings = variable_get('webform_addmore_' . $component['nid'], array());
  $settings[$component['cid']] = array(
    'enabled' => $component['extra']['addmore'],
    'button' => array(
      'add' => $component['extra']['addmore_settings']['add_button'],
      'remove' => $component['extra']['addmore_settings']['remove_button'],
    ),
  );
  variable_set('webform_addmore_' . $component['nid'], $settings);
}

 /**
 * Delete webform component.
 *
 * @param $component
 *   The Webform component being deleted.
 */
function webform_addmore_webform_component_delete($component) {
  if ($component['type'] != 'fieldset') {
    return;
  }
  $settings = variable_get('webform_addmore_' . $component['nid'], array());
  unset($settings[$component['cid']]);
  variable_set('webform_addmore_' . $component['nid'], $settings);
}

/**
 * Implements hook_form_alter().
 *
 * Find form_key from cid and add JS.
 */
function webform_addmore_form_alter(&$form, &$form_state, $form_id) {
  if (substr($form_id, 0, 19) != 'webform_client_form') {
    return;
  }

  // Find form_key from cid.
  $form_key = array();
  $settings = variable_get('webform_addmore_' . $form['#node']->nid, array());

  foreach ($settings as $cid => $setting) {
    if (is_int($cid) && $setting['enabled']) {
      _webform_addmore_form_alter($form, $form_state, $cid, $setting);
    }
  }
}

function webform_addmore_webform_submission_render_alter(&$renderable) {
  module_load_include('inc', 'webform', 'includes/webform.components');

  $settings = variable_get('webform_addmore_' . $renderable['#node']->nid, array());

  foreach ($settings as $cid => $setting) {
    if (is_int($cid) && $setting['enabled']) {
      $parents = webform_component_parent_keys($renderable['#node'], $renderable['#node']->webform['components'][$cid]);

      $multiple_fieldset =& drupal_array_get_nested_value($renderable, $parents);

      foreach (element_children($multiple_fieldset) as $element) {
        $cid = $multiple_fieldset[$element]['#webform_component']['cid'];
        $multiple_values = theme('item_list', array('items' => $renderable['#submission']->data[$cid]['value']));
        $multiple_fieldset[$element]['#format'] = 'list';
        $multiple_fieldset[$element]['#value'] = $multiple_values;
      }
    }
  }
}

function _webform_addmore_form_alter(&$form, &$form_state, $cid, $setting) {
  module_load_include('inc', 'webform', 'webform.components');

  $submission = $form_state['build_info']['args'][1];
  $node = $form_state['build_info']['args'][0];

  $parents = webform_component_parent_keys($form['#node'], $form['#node']->webform['components'][$cid]);

  $component_id = str_replace('-', '_', implode('_', $parents));

  $multiple_fieldset =& drupal_array_get_nested_value($form['submitted'], $parents);

  if (empty($form_state[$cid]['num_names'])) {
    $element = element_children($multiple_fieldset);
    $element = reset($element);
    if (isset($submission->data[$multiple_fieldset[$element]['#webform_component']['cid']]['value'])) {
      $form_state[$cid]['num_names'] = count($submission->data[$multiple_fieldset[$element]['#webform_component']['cid']]['value']);
    }
    else {
      $form_state[$cid]['num_names'] = 1;
    }
  }

  foreach (element_children($multiple_fieldset) as $element) {
    $field = $multiple_fieldset[$element];
    $multiple_fieldset[$element] = array();
    for ($i = $form_state[$cid]['num_names']; $i > 0; $i--) {
      $field['#weight'] = $i - 1;
      if (isset($submission->data[$field['#webform_component']['cid']]['value'][$i-1])) {
        $field['#default_value'] = $submission->data[$field['#webform_component']['cid']]['value'][$i-1];
      }
      else {
        $field['#default_value'] = '';
      }

      if ($form_state[$cid]['num_names'] > 1) {
        $placeholder = t('@title (@count of @num)'
          , array('@title' => $field['#title']
            , '@count' => $i
            , '@num' => $form_state[$cid]['num_names']));
      }
      else {
        $placeholder = $field['#title'];
      }

      $field['#placeholder'] = $placeholder;
      $multiple_fieldset[$element][$i-1] = $field;
    }
  }

  $multiple_fieldset['#prefix'] = '<div id="' . $component_id . '-fieldset-wrapper">';
  $multiple_fieldset['#suffix'] = '</div>';

  $multiple_fieldset['add_name'] = array(
    '#type' => 'submit',
    '#name' => $component_id . '_add_name',
    '#value' => $setting['button']['add'],
    '#submit' => array('webform_addmore_add_more_add_one'),
    '#limit_validation_errors' => array(),
    '#attributes' => array('class' => array('add_more')),
    // See the examples in ajax_example.module for more details on the
    // properties of #ajax.
    '#ajax' => array(
      'callback' => 'webform_addmore_add_more_callback',
      'wrapper' => $component_id . '-fieldset-wrapper',
    ),
    '#prefix' => '<span>+</span>',
    '#weight' => 98,
  );

  if ($form_state[$cid]['num_names'] > 1) {
    $multiple_fieldset['remove_name'] = array(
      '#type' => 'submit',
      '#name' => $component_id . '_remove_name',
      '#value' => $setting['button']['remove'],
      '#submit' => array('webform_addmore_add_more_remove_one'),
      '#limit_validation_errors' => array(),
      '#attributes' => array('class' => array('remove_one')),
      '#ajax' => array(
        'callback' => 'webform_addmore_add_more_callback',
        'wrapper' => $component_id . '-fieldset-wrapper',
      ),
      '#prefix' => '<span>-</span>',
      '#weight' => 99,
    );
  }
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function webform_addmore_add_more_callback($form, $form_state) {
  $button = $form_state['triggering_element'];

  // Go one level up in the form, to the widgets container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  return $element;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function webform_addmore_add_more_add_one($form, &$form_state) {
  $button = $form_state['triggering_element'];

  // Go one level up in the form, to the widgets container.
  $element_parents = array_slice($button['#array_parents'], 0, -1);
  $element = drupal_array_get_nested_value($form, $element_parents);
  $cid = $element['#webform_component']['cid'];

  $form_state[$cid]['num_names']++;

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function webform_addmore_add_more_remove_one($form, &$form_state) {
  $button = $form_state['triggering_element'];

  // Go one level up in the form, to the widgets container.
  $element_parents = array_slice($button['#array_parents'], 0, -1);
  $element = drupal_array_get_nested_value($form, $element_parents);
  $cid = $element['#webform_component']['cid'];

  if ($form_state[$cid]['num_names'] > 1) {
    $form_state[$cid]['num_names']--;
  }

  $form_state['rebuild'] = TRUE;
}

