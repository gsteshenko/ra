<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */

?>
<?php
  if (isset($row->field_field_contributor_bio_summary[0]['rendered']['#markup'])) {
    $bio = $row->field_field_contributor_bio_summary[0]['rendered']['#markup'];
    $expand = '... ' . l('Show Full Bio', '', array('fragment' => 'full-bio'
      , 'attributes' => array('class' => 'full-bio'))) ;

    $output = ra_alters_replace_last_word($bio, $expand);
  }

  print $output;
