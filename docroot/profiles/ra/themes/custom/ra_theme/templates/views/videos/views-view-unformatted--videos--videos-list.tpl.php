<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>

<?php
  $node = menu_get_object();
  $node_wrapper = entity_metadata_wrapper('node', $node);
?>

<?php if ('28' == $node_wrapper->field_video_category->value()->tid): ?>
  <div class="views-row views-row-last more-handy-home-tips">
    <div class="views-field views-field-field-image">
      <div class="field-content video-thumb-placeholder">
        <?php print l('Around the Home', 'ra-management-around-the-home'); ?>
      </div>
    </div>
    <div class="views-field views-field-title">
      <div class="field-content">
        <?php print l('More handy home tips', 'ra-management-around-the-home'); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
