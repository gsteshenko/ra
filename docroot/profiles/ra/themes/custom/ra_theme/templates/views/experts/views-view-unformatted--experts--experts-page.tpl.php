<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="views-row views-row-0">
<div class="experts-placeholder"><div class="experts-table"><div class="experts-cell">Meet the <span>Experts</span></div></div></div>
<?php
    $img = array(
      'path' => drupal_get_path('theme', 'ra_theme') . '/images/spacer.gif',
    );
    print theme('image', $img);
?>
</div>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>

