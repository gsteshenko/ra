var RA;

(function($) {
  RA = {
    settings: {
      slider: {
        '.slider-default.flexslider': {
          animation: 'slide'
        },
        '.slider-joints.flexslider': {
          slider_id: "joints",
          animation: "slide",
          prevText: "Previous Slide",
          nextText: "Next Slide",
          animationLoop: false,
          slideshow: false,
          start: function(slider) {
            RA.callbacks.callSliderPager(slider, 'init', this);
          },
          after: function(slider) {
            RA.callbacks.callSliderPager(slider, 'slide', this);
          }
        }
      }
    },
    callbacks: {
      callSliderPager: function(slider, action, o) {
        if (typeof slider.container !== 'undefined') {
          $.each(slider.container, function(index, container) {
            var $container = $(container)
              , wrapper_numpager = $container.parents('div.view').find('.numpager')
              , wrapper_slide_title = $container.parents('div.view').find('.slide-title')
              , header_left = $container.find('.flex-active-slide h2').text()
              , header_right = Drupal.t('Slide')
                + ' ' + (slider.currentSlide + 1) + '/' + slider.pagingCount;

            if (action == 'init') {
              $(slider.context).find('.flex-direction-nav')
                .clone(true)
                .appendTo(slider.context)
                .addClass('flex-bottom-nav');
            }
            if (action = 'slide') {
              $('body').trigger(o.slider_id + 'SliderSlide', [$(slider.context).find('.flex-active-slide')]);

              $(slider.context).find('.flex-bottom-nav').remove();
              $(slider.context).find('.flex-direction-nav')
                .clone(true)
                .appendTo(slider.context)
                .addClass('flex-bottom-nav');
            }
            wrapper_numpager.html(header_right);
            wrapper_slide_title.html(header_left);
          });
        }
      },

      callPledgeAssign: function() {
        $("a.pledge-share").click(function() {
          return !window.open(pledge_fshare_url, "Facebook", "width=640,height=300");
        });
      },

      callMenuHover: function() {
        $('.menu li').hover(function() {
          $(this).find('>a').toggleClass('hover');
        }, function() {
          $(this).find('>a').toggleClass('hover');
        });
      },

      callInitSlider: function() {
        $.each(RA.settings.slider, function(key, settings) {
          var target = $(key);
          target.flexslider(settings);
        });

        // Stop sliding while mobile menu is opened.
        $('#toggle-menu').click(function() {
          $.each(RA.settings.slider, function(key, settings) {
            $(key).flexslider($('body').hasClass('menu-open') ? 'pause' : 'play');
          });
        });
      },

      callInitOverlay: function() {
        var overlay_top_offset = parseInt($('.overlay-white').css('top'), 10);
        $(window).scroll(function() {
          var overlay_offset = overlay_top_offset - $('body').scrollTop();
          $('.overlay-white').css('top', overlay_offset > 0 ? overlay_offset : 0);
        });
      },

      callSearchTrigger: function() {
        var target = $('div#mini-panel-header_menus div.pane-views.pane-articles')
          , button = $('li.menu-item-1232')
          ;
        if ('searchPopupFirstCall' in Drupal) {
          Drupal.searchPopupFirstCall = false;
        }
        else {
          Drupal.searchPopupFirstCall = true;
        }
        Drupal.searchPopupFirstCall && $('.view-empty', target).hide();
        button.add('.view-display-id-search_articles a.button-close').unbind('click').click(function() {
          button.add('span', button).toggleClass('active');
          target.is(':visible') ? target.hide() : target.show().find('input[name="field_search_value"]').focus();
          return false;
        });
      },

      callBodyPager: function() {
        var body_texts = $(".node-type-article .field-name-field-article-body");
        if (body_texts.length
          && body_texts.find('.field-items .field-item').length > 1) {
          body_texts.append('<div class="pager_wrapper">'
            + '<div class="info_text"></div>'
            + '<div class="page_navigation"></div></div>');
          body_texts.pajinate({
            items_per_page : 1,
            item_container_id : '.field-items',
            show_first_last : false,
            nav_label_prev: 'Previous',
            nav_order: ["first", "prev", "num", "next", "last"],
            nav_label_info: '{3} of {4}'
          });
          $('.page_navigation a').click(function() {
            $('html, body').animate({
              scrollTop: $(".pane-node-field-article-body").offset().top
            }, 200);
          });
        }
      },

      callCloseSignup: function() {
        $('.signup-block').find('.signup-close-btn a').click(function() {
          $(this).parents('.signup-block').fadeOut();
        });
      },

      callScrollCustom: function() {
        $('.nano > div').addClass('content');
        $('.nano').nanoScroller({ alwaysVisible: true });
      },

      callCustomForms: function() {
        jQuery(".webform-client-form input[type='radio'][value=0]").attr("checked",true);
        $('.webform-client-form').find('input:not(.add_more,.remove_one), textarea, select, button').uniform();
        jQuery(".webform-client-form select.error").parent().addClass('form-error');
      },

      callMobileDeviceFixes: function() {
        // Add notouch class to recipe menu items
        $('.pane-menu-menu-recipe-menu .menu-depth-1').addClass('notouch');

        if ($.browser.mobile) {
          $('.notouch').removeClass('notouch').click(function() {
            var el = $(this);

            el.siblings().removeClass('hover');
            el.toggleClass('hover');
          });
        }
      },

      callPlaceholderFix: function() {
        $('input, textarea').placeholder();
      },

      callBindCustomClose: function() {
        $('.popup-close').click(function(e) {
          e.preventDefault();
          Drupal.CTools.Modal.dismiss();
        });;
      },

      callInspirationSocial: function() {
        $('a.insp-share-all').click(function(e) {
          e.preventDefault();
          $('.social-wrapper').slideToggle();
        });
      },

      callCtoolsModalResize: function() {
        $('.ctools-modal-content, .ctools-modal-content .modal-content').each(function() {
          $(this).css({'min-height': $(this).css('height')});
        }).css('height', '');
      },

      callPollResults: function() {
        var context = $('.view-polls')
          , row = $('.pollfield-row', context)
          , form = $('#pollfield-voting-form', context)
          , back = $('.poll-back', context)
          , results = $('a.poll-view-results', context)
          ;
        results.click(function(e) {
          e.preventDefault();
          row.css('display', 'table');
          form.add(back).toggle();
        });
        $(back).click(function(e) {
          e.preventDefault();
          row.hide();
          form.add(back).toggle();
        });
        if (!$('.form-item', context).length) {
          row.css('display', 'table');
          back.add(results).hide();
        }
      },

      callSetPlaceholderAttribute: function() {
        //sets the attribute "placeholder" on the search form
        //Functional effect: changes <input id="edit-field-search-value"> to <input id="edit-field-search-value" placeholder="search">
        //@todo do this with HTML, JS is a a last minute hack
        jQuery('#views-exposed-form-articles-search-articles input#edit-field-search-value').attr('placeholder', 'Search');
      },

      callMobileTable: function() {
        if ($.browser.mobile && $(window).width() < 420) {
          $('.node-type-article .pane-node-field-article-body table').each(function() {
            var desktopTable = $(this);
            if (desktopTable.length) {
              var headers = [];
              var rows = [];

              desktopTable.find('th').each(function(index) {
                headers.push('<span class="table-mobile-header">' + $(this).text() + ': ' + '</span>');
              });

              desktopTable.find('tbody tr').each(function() {
                var cells = [];
                $(this).find('td').each(function() {
                  cells.push($(this).html());
                });
                rows.push(cells);
              });

              var tableMobile = $('<div class=table-mobile/>').insertAfter(desktopTable);

              var output = [];
              rows.forEach(function(row) {
                output.push('<div>');
                row.forEach(function(cell, i) {
                  output.push('<div>' + headers[i] + cell + '</div>');
                });
                output.push('</div>');
              });
              tableMobile.append(output.join(''));
              desktopTable.remove();
            }
          });
        }
      },

      callShowFullBio: function() {
        $('.view-display-id-video_contributors a.full-bio, .view-display-id-video_contributors a.minimize').click(function(e) {
          e.preventDefault();
          var parent = $(this).parents('.views-row');
          parent.find('.views-field-body').toggle();
          parent.find('.views-field-body-1').toggle();
        });
        // Show Full Bio link handler for mobile devices
        $('.view-display-id-video_contributors a.full-bio-mobile').click(function(e) {
          e.preventDefault();
          var parent = $(this).parents('.views-row');
          $(this).toggle();
          parent.find('.views-field-body-2').css('max-height', '100%');
          parent.find('a.minimize-mobile').toggle();
        });
        $('.view-display-id-video_contributors a.minimize-mobile').click(function(e) {
          e.preventDefault();
          var parent = $(this).parents('.views-row');
          $(this).toggle();
          parent.find('.views-field-body-2').css('max-height', '66px');
          parent.find('a.full-bio-mobile').toggle();
        });
      },

      callTextUpdates: function() {
        $('.node-type-video .print').html('Print article');
      },

      callUpdateWelcomeDate: function() {
        var welcome_block = $('div.welcome-block div.welcome-date');

        if (!welcome_block.length) {
          return;
        }

        var monthes = ["January", "February", "March", "April", "May", "June"
            , "July", "August", "September", "October", "November", "December"]
          , weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"
            , "Friday", "Saturday"]
          , date = new Date()
          , day = ('0' + date.getDate()).slice(-2)
          , month = Drupal.t(monthes[date.getMonth()])
          , weekday = Drupal.t(weekdays[date.getDay()]);

          welcome_block.html(weekday + ' <span>' + month + ' ' + day + '</span>');
      }
    }
  }

  $(window).load(function() { });

  $(function() {
    RA.callbacks.callUpdateWelcomeDate();
    // Moved flexslider init under document ready because of omniture long loading.
    RA.callbacks.callInitSlider();

    RA.callbacks.callMobileDeviceFixes();
    RA.callbacks.callCloseSignup();
    RA.callbacks.callMenuHover();
    RA.callbacks.callScrollCustom();
    RA.callbacks.callBodyPager();
    RA.callbacks.callInitOverlay();
    RA.callbacks.callPlaceholderFix();
    RA.callbacks.callInspirationSocial();
    RA.callbacks.callPollResults();
    RA.callbacks.callSetPlaceholderAttribute();
    RA.callbacks.callMobileTable();
    RA.callbacks.callShowFullBio();
    RA.callbacks.callTextUpdates();
  });

  Drupal.behaviors.ra = {
    attach: function(context, settings) {
      RA.callbacks.callCustomForms();
      RA.callbacks.callPledgeAssign();
      RA.callbacks.callCtoolsModalResize();
      RA.callbacks.callSearchTrigger();
      RA.callbacks.callBindCustomClose();
    }
  };
}) (jQuery);
