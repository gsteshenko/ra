<?php
/**
 * Empty settings.php file for the "default" site
 * This file will prevent install.php from being invoked for a domain/site that is not defined.
 * This file is also required for proper sites/all/files symlink provisioning in Acquia Cloud
 *
 * DO NOT REMOVE!
 * */
