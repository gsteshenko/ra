<?php
/**
 * This settings.php file was created by the Acquia Cloud ah-site-archive-import
 * Drush command. The imported archive did not contain a settings.php file, so
 * the import process created this file by default. You can replace this file
 * with the standard default settings.php file for your version of Drupal.
 * However, be sure to keep the last line that loads the "Acquia Cloud settings
 * include file", which provides the correct database credentials for your site.
 */
$update_free_access = FALSE;
$drupal_hash_salt = '';
ini_set('arg_separator.output',     '&amp;');
ini_set('magic_quotes_runtime',     0);
ini_set('magic_quotes_sybase',      0);
ini_set('session.cache_expire',     200000);
ini_set('session.cache_limiter',    'none');
ini_set('session.cookie_lifetime',  2000000);
ini_set('session.gc_divisor',       100);
ini_set('session.gc_maxlifetime',   200000);
ini_set('session.gc_probability',   1);
ini_set('session.save_handler',     'user');
ini_set('session.use_cookies',      1);
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid',    0);
ini_set('url_rewriter.tags',        '');

# Acquia configuration
if (file_exists('/var/www/site-php')) {
  require('/var/www/site-php/blinkdev3/blinkdev3-settings.inc');
}

# Error reporting level configuration
$conf['error_level'] = 0; // Error messages to display: 0 - None; 1 - Errors and warnings; 2 - All messages
ini_set("display_errors", "0"); // do not display errors
error_reporting(E_ALL & ~E_NOTICE);  // ignore PHP Notices

# File system configuration
$conf['file_public_path'] = 'sites/all/files';
$conf['file_private_path'] = '';
$conf['file_temporary_path'] = '/tmp';

# Drupal Performance configuration
$conf['cache'] = TRUE;
$conf['block_cache'] = TRUE;
$conf['cache_lifetime'] = 3600 * 3;
$conf['page_cache_maximum_age'] = 3600 * 3;
$conf['page_compression'] = TRUE;
$conf['preprocess_css'] = TRUE;
$conf['preprocess_js'] = TRUE;

# Environment Modules configuration - enable the following modules on DEV/TEST
$conf['environment_modules'] = array(
  //'shield' => './profiles/sit/modules/contrib/shield/shield.module',
);

# Shield Module configuration - disable in production environment
// $conf['shield_allow_cli'] = TRUE;
// $conf['shield_user'] = '';
// $conf['shield_pass'] = '';

# Memcache configuration
$conf['cache_backends'][] = './profiles/ra/modules/contrib/memcache/memcache.inc';
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';

# SOAP client configuration
$conf['ra_soap_interaction_prefix'] = 'QRSBUNDLETEST';

# Varnish configuration
//$conf['cache_backends'][] = './profiles/ra/modules/contrib/varnish/varnish.cache.inc';
//$conf['cache_class_cache_page'] = 'VarnishCache';
//$conf['reverse_proxy'] = TRUE;
//$conf['reverse_proxy_addresses'] = array('a.b.c.d');
//$conf['varnish_control_terminal'] = 'a.b.c.d:6082';

// Omniture s_account configuration
$conf['ra_omniture_account'] = 'genedev';
